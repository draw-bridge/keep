namespace DrawBridge

open DrawBridge.Common
open DrawBridge.Common.Types
open DrawBridge.Common.Extensions
open DrawBridge.Drawing
open DrawBridge.Plotter.JsonRpc.V1

#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

type RunnerError =
    | SerdeError of string
    | SocketError of Socket.SocketError
    | PlotterError of ApiError
    | EnvVarError of string

    static member OfDecodeError de = SerdeError(de)
    static member OfSocketError se = SocketError(se)
    static member OfApiError ae = PlotterError(ae)

    static member BindDecodeError de =
        de |> RunnerError.OfDecodeError |> Error

    static member BindSocketError se =
        se |> RunnerError.OfSocketError |> Error

    static member BindApiError ae = ae |> RunnerError.OfApiError |> Error

    override this.ToString() =
        match this with
        | SerdeError de -> $"Hit a serde error: {de}"
        | SocketError se -> $"Hit a socket error: {se}"
        | PlotterError ae -> $"Hit a Plotter error: {ae}"
        | EnvVarError eve -> $"Hit an env var error: {eve}"

type Runner =
    { SocketPath: string }

    /// Try to create a new runtime by reading env vars to get a socket path.
    static member NewFromEnv() =
        match Functions.readEnvVar Environment.DrawBridgeSocketEnvVar with
        | Some path -> Ok { SocketPath = path }
        | None ->
            $"Failed to read socket path at env var {Environment.DrawBridgeSocketEnvVar}"
            |> EnvVarError
            |> Error

    static member New socketPath = { SocketPath = socketPath }

    // TODO[gastove|2020-12-30] This needs to do a variety of checks:
    // - [ ] Can we talk to the plotter at all?
    // - [ ] Does the plotter have enough voltage?
    // - [ ] Probably others.
    member this.PlotterReady _ : Result<Runner, RunnerError> = Ok(this)

    member private __.ReadResponse(socket: Socket.ISocket) = Socket.receive socket

    member private __.ParseResponse bytes =
        let response: Result<Response, RunnerError> =
            bytes
            |> Convert.StringFromBytes
            |> Decode.fromString Response.decoder
            |> Result.bindError RunnerError.BindDecodeError

        response
        |> Result.bind (fun r -> r.AsResult() |> Result.bindError RunnerError.BindApiError)

    member private __.SendAndReceive bytes (socket: Socket.ISocket) =
        socket |> Socket.send bytes |> Result.bind (fun _ -> Socket.receive socket)

    /// Entrypoint for reporting Plotter Status to the user
    member this.ReportPlotterStatus plotter =
        let cmd = GetConfig
        let paramz = PlotterOnly plotter

        { Id = Some 0
          Method = cmd
          Params = paramz }
        |> Request.encoder
        |> Encode.toString 0
        |> Convert.BytesFromString
        |> this.DoRPC

    member private this.DoRPC bytes =
        use socket = Socket.newSocket this.SocketPath :> Socket.ISocket |> Socket.openSocket

        socket
        |> DisposableResult.BindIntoResult(this.SendAndReceive bytes)
        |> Result.bindError RunnerError.BindSocketError
        |> Result.bind this.ParseResponse

    member private __.ToJsonRpc commands : Request list =
        commands |> List.map (fun (cmd, paramz) -> Request.Create None cmd paramz)


    member this.Run(drawing: Drawing) =
        // TODO[gastove|2022-07-10] At some point, I'll implement scale and
        // need to update this with something more sensible *or* will remove
        // scale and this should be updated.
        // let plotterRenderer =
        //     Rendering.PlotterRenderer.Create 1 drawing.Plotter

        let rpc =
            drawing.Shapes
            |> List.map Rendering.PlotterRenderer.Default.Render
            |> List.concat
            |> this.ToJsonRpc

        let bytes = rpc |> List.map (Request.encoder >> Convert.JsonToBytes)

        // TODO[gastove|2020-12-27] This needs a couple improvements:
        // - [ ] We should really capture the failing command for debuigging
        // - [ ] We need to compare length more accurately, but that means I
        //       need to change the way I add message terminators.
        let rec run cmds (acc: ApiResult list) =
            match cmds with
            | cmd :: rest ->
                this.DoRPC cmd
                |> Result.bind (fun resultString -> List.append [ resultString ] acc |> run rest)

            | [] -> Ok(acc)

        run bytes List.empty

    member this.GetConfig(plotter: Plotter) =
        let cmd = Queries.Config plotter

        cmd |> Request.encoder |> Convert.JsonToBytes |> this.DoRPC

module Runner =
    let run drawing (runner: Runner) = runner.Run(drawing)

    let getConfig plotter (runner: Runner) = runner.GetConfig plotter
    let reportPlotterStatus plotter (runner: Runner) = plotter |> runner.ReportPlotterStatus
