namespace DrawBridge.Drawing

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Geometry =
    open System
    
#if FABLE_COMPILER
    open Thoth.Json
#else
    open Thoth.Json.Net
#endif

    open DrawBridge

    module Delta =
        let encoder (delta: Delta) =
            Encode.object [ "x_distance", Encode.int delta.XDistance
                            "y_distance", Encode.int delta.YDistance ]

        let decoder: Decoder<Delta> =
            Decode.object (fun get ->
                { XDistance = get.Required.Field "x_distance" Decode.int
                  YDistance = get.Required.Field "y_distance" Decode.int })


    module Point =
        let encoder (point: Point) =
            Encode.object [ "x", Encode.int point.X
                            "y", Encode.int point.Y ]

        let decoder: Decoder<Point> =
            Decode.object (fun get ->
                { X = get.Required.Field "x" Decode.int
                  Y = get.Required.Field "y" Decode.int })

    /// The direct (non-Manhattan) distance between two points:
    /// d = sqrt((x2 - x1)^s + (y2 - y1)^2)
    let distanceBetween (pt1: Point) (pt2: Point) =
        // Save a small amount of math using the Delta
        let delta = pt1.Delta pt2

        let x = Math.Pow(float delta.XDistance, 2.0)
        let y = Math.Pow(float delta.YDistance, 2.0)

        Math.Sqrt(x + y) |> round |> int

    /// Compute the side length of the two legs of an isosclese right triangle
    /// given the points that delimit the hypotenuse. The ratio of the sides of
    /// a 45°x45°x90° triangle is 1:1:sqrt(2), so I should be able to divide by
    /// the square root of 2 to get the side length.
    let computeIsoclesRightTriangleSideLength (pt1: Point) (pt2: Point) =
        let hypotenuseLength = distanceBetween pt1 pt2

        (float hypotenuseLength) / Math.Sqrt(2.0)
        |> round
        |> int
