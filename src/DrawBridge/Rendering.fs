namespace DrawBridge

module Rendering =
    
    open DrawBridge.Common.Types    
    open DrawBridge.Drawing.Geometry    
    open DrawBridge.Plotter.JsonRpc.V1
    
    /// Implementation details for rendering. Need to be visible for testing,
    /// somehow, because Very Important, but I need a way to indicate that these
    /// are parts other code shouldn't be reaching for.
    module Internal =

        type PlotterDirection = Method * MethodParams

        /// Internal type used for converting a Drawing to a set of plotter Commands.
        type RenderState<'ElementType> =
            { PenPosition: Point
              State: array<'ElementType> }

            static member Create point commands =
                { PenPosition = point
                  State = commands }

            static member Default =
                { PenPosition = Point.Default()
                  State = Array.empty }

            /// Updates the RenderState with a set of commands and the pen
            /// position once those commands are executed.
            member this.Update currentPenPosition cmds =
                let commands = Array.append this.State cmds

                RenderState<'ElementType>.Create currentPenPosition commands

        type PlotterState = RenderState<PlotterDirection>

        let directionsForPoint (state: PlotterState) (plotter: Plotter) (point: Point) =
            let plotterOnly = plotter |> PlotterOnly

            let delta = state.PenPosition.Delta point

            // A delta producing no movement is a noop, omit.
            if delta.Zero then
                state
            else
                let gotoParams = MovePenParams(plotter, [ delta ], None)

                let commands =
                    [| MovePen, gotoParams
                       LowerPen, plotterOnly
                       RaisePen, plotterOnly |]

                state.Update point commands

        let directionsForLine (state: PlotterState) (plotter: Plotter) ((lineStart: Point), (lineEnd: Point)) =
            if lineStart = lineEnd then
                directionsForPoint state plotter lineEnd
            else
                let plotterOnly = plotter |> PlotterOnly

                let gotoParams =
                    let delta = state.PenPosition.Delta lineStart
                    MovePenParams(plotter, [ delta ], None)

                let drawParams =
                    let delta = lineStart.Delta lineEnd
                    MovePenParams(plotter, [ delta ], None)

                let commands =
                    [| MovePen, gotoParams
                       LowerPen, plotterOnly
                       MovePen, drawParams
                       RaisePen, plotterOnly |]

                state.Update lineEnd commands

        let directionsForDrawable (plotter: Plotter) (state: PlotterState) (drawable: Drawable) =
            match drawable with
            | Drawable.Point point -> directionsForPoint state plotter point
            | Drawable.Line points -> directionsForLine state plotter points

    // TODO[gastove|2022-07-09] So, in theory, I have the notion of scale, and a
    // notion of maximum drawing area. This strikes me as the right place to
    // handle that, and the considerably simpler rendering approach feels to me
    // like it should make that easier. I really hope that's true.
    type PlotterRenderer =
        { Scale: float
          Plotter: Plotter }

        static member Default =
            PlotterRenderer.Create 1 Plotter.AxiDrawV3A3

        static member Create scale plotter = { Scale = scale; Plotter = plotter }

        member this.Render(drawable: IDrawable) =

            let componentsToRender = drawable.Draw()

            let final =
                componentsToRender
                |> Seq.fold (Internal.directionsForDrawable this.Plotter) Internal.PlotterState.Default

            // Compute a final move back to home
            let homeDelta = final.PenPosition.Delta(Point.Default())

            let homeMoveParams =
                MovePenParams(this.Plotter, [ homeDelta ], None)
 
            let commands =
                Array.append final.State [| MovePen, homeMoveParams |]

            commands |> Array.toList

    // TODO[gastove|2024-08-06] This does not *at all* belong in here.
    // - Create an IRenderer interface that's public
    // - Keep can hold the implementation of it.
    //
    // module SVG =
    //     open Giraffe.ViewEngine

    //     // Tags that can carry additional content
    //     let svg = tag "svg"

    //     // void tags -- tags that can't carry additional content
    //     let line = voidTag "line"

    //     // Attributes
    //     let x1 = attr "x1"
    //     let x2 = attr "x2"
    //     let y1 = attr "y1"
    //     let y2 = attr "y2"

    //     let renderLine (incoming: Line) =
    //         match incoming with
    //         | Straight (lineStart, lineEnd) ->
    //             line [ x1 <| string lineStart.X
    //                    y1 <| string lineStart.Y
    //                    x2 <| string lineEnd.X
    //                    y2 <| string lineEnd.Y ]

    //     // Manually rendering a Rectangle like this, using lines, allows us to
    //     // much more easily draw rotated quadrangles.
    //     let renderRectangle (rect: Rectangle) =
    //         [ // Upper Left to Upper Right
    //           line [ x1 <| string rect.UpperLeft.X
    //                  y1 <| string rect.UpperLeft.Y
    //                  x2 <| string rect.UpperRight.X
    //                  y2 <| string rect.UpperRight.Y ]
    //           // Upper Right to Lower Right
    //           line [ x1 <| string rect.UpperRight.X
    //                  y1 <| string rect.UpperRight.Y
    //                  x2 <| string rect.LowerRight.X
    //                  y2 <| string rect.LowerRight.Y ]
    //           // Lower Right to Lower Left
    //           line [ x1 <| string rect.LowerRight.X
    //                  y1 <| string rect.LowerRight.Y
    //                  x2 <| string rect.LowerLeft.X
    //                  y2 <| string rect.LowerLeft.Y ]
    //           //  Lower Left to Upper Left
    //           line [ x1 <| string rect.LowerLeft.X
    //                  y1 <| string rect.LowerLeft.Y
    //                  x2 <| string rect.UpperLeft.X
    //                  y2 <| string rect.UpperLeft.Y ] ]
    //         |> div []

    //     // Point doesn't currently implement IRenderable, so we can't actually
    //     // draw one, which sort of makes sense. Instead, we're making a line
    //     let renderPoint (incoming: Point) =
    //         line [ x1 <| string incoming.X
    //                y1 <| string incoming.Y
    //                x2 <| string incoming.X
    //                y2 <| string incoming.Y ]

    //     let renderSvgElement (renderable: IRenderable) =
    //         match renderable with
    //         | :? Line as line -> line |> renderLine

    //         | :? Rectangle as rect -> rect |> renderRectangle

    //         | wrong -> failwith $"Look, I just don't even know: {wrong}"

    // // TODO[gastove|2022-12-27] OK this is gonna be Weird for sure: with SVG
    // // elements, everything needs to be wrapped up in a final package, which
    // // means the overall interface is slightly different -- not quite so "render
    // // one element, over and over", more, "render this whole thing." It may be
    // // that having a single Renderer type isn't the correct plan.
    // type SvgRenderer =
    //     { Scale: float }

    //     static member Create scale = { Scale = scale }

    //     member _.Render(renderables: IRenderable seq) =
    //         renderables
    //         |> Seq.map SVG.renderSvgElement
    //         |> Seq.toList
    //         |> SVG.svg []
    //         |> Giraffe.ViewEngine.RenderView.AsString.htmlNode

    // module SvgRenderer =

    //     let renderToSvg (renderable: Shapes.IRenderable) =
    //         renderable.Lines()
    //         |> Seq.map (function
    //             | Shapes.Drawable.Point pt ->
    //                 SVG.line [ SVG.x1 <| string pt.X
    //                            SVG.y1 <| string pt.Y
    //                            SVG.x1 <| string pt.X
    //                            SVG.y1 <| string pt.Y ]

    //             | Shapes.Drawable.Line (pt1, pt2) ->
    //                 SVG.line [ SVG.x1 <| string pt1.X
    //                            SVG.y1 <| string pt1.Y
    //                            SVG.x1 <| string pt2.X
    //                            SVG.y1 <| string pt2.Y ])
    //         |> Seq.toList

    //     let renderToSvgDocument (renderables: Shapes.IRenderable list) =
    //         let elements = renderables |> List.collect renderToSvg

    //         SVG.svg [] elements
    //         |> Giraffe.ViewEngine.RenderView.AsString.htmlNode
