namespace DrawBridge

open DrawBridge.Common.Types

type Drawing =
    { Id: int
      Dimensions: Dimensions
      Shapes: IDrawable list
      Plotter: Plotter
      Error: string option }

    static member Create id dim shapes plotter err =
        { Id = id
          Dimensions = dim
          Shapes = shapes
          Plotter = plotter
          Error = err }

module Drawing = 
         
    let draw (renderer: IRenderer<'Output>) (drawing: Drawing)=
        ()
 
    module Geometry =
        open System
        #if FABLE_COMPILER
            open Thoth.Json
        #else
            open Thoth.Json.Net
        #endif

        open DrawBridge

        module Delta =
            let encoder (delta: Delta) =
                Encode.object [ "x_distance", Encode.int delta.XDistance
                                "y_distance", Encode.int delta.YDistance ]

            let decoder: Decoder<Delta> =
                Decode.object (fun get ->
                    { XDistance = get.Required.Field "x_distance" Decode.int
                      YDistance = get.Required.Field "y_distance" Decode.int })


        module Point =
            let encoder (point: Point) =
                Encode.object [ "x", Encode.int point.X
                                "y", Encode.int point.Y ]

            let decoder: Decoder<Point> =
                Decode.object (fun get ->
                    { X = get.Required.Field "x" Decode.int
                      Y = get.Required.Field "y" Decode.int })

        /// The direct (non-Manhattan) distance between two points:
        /// d = sqrt((x2 - x1)^s + (y2 - y1)^2)
        let distanceBetween (pt1: Point) (pt2: Point) =
            // Save a small amount of math using the Delta
            let delta = pt1.Delta pt2

            let x = Math.Pow(float delta.XDistance, 2.0)
            let y = Math.Pow(float delta.YDistance, 2.0)

            Math.Sqrt(x + y) |> round |> int

        /// Compute the side length of the two legs of an isosclese right triangle
        /// given the points that delimit the hypotenuse. The ratio of the sides of
        /// a 45°x45°x90° triangle is 1:1:sqrt(2), so I should be able to divide by
        /// the square root of 2 to get the side length.
        let computeIsoclesRightTriangleSideLength (pt1: Point) (pt2: Point) =
            let hypotenuseLength = distanceBetween pt1 pt2

            (float hypotenuseLength) / Math.Sqrt(2.0)
            |> round
            |> int


    module Shapes =

        open System
        open DrawBridge
        open Geometry

        [<Literal>]
        let CircleScalingFactor = 100

        type Line =
            | Straight of beginning: Point * ending: Point

            static member CreateStraight s e = Line.Straight(s, e)
            // TODO[gastove|2022-07-10] This will be very nice, write it.
            static member Compute (origin: Point) (length: int) (direction: float) = ()

            interface IDrawable with
                member this.Draw() : seq<Drawable> =
                    match this with
                    | Straight (b, e) -> seq { Drawable.Line(b, e) }

        /// Represents a Rectangle. Really, can represent any quadrangle --
        /// rectangle, square parallelogram, just a quadrangle.
        type Rectangle =
            { UpperLeft: Point
              UpperRight: Point
              LowerLeft: Point
              LowerRight: Point }

            static member Create upperLeft upperRight lowerLeft lowerRight =
                { UpperLeft = upperLeft
                  UpperRight = upperRight
                  LowerLeft = lowerLeft
                  LowerRight = lowerRight }
            /// Create a square given two control points, the "upper left" and
            /// "lower right" corners. A Square is a special case of Rectangle, and
            /// renders exactly the same, so I'm modeling with the same underlying
            /// data type. I may split this out for conceptual clarity later.
            static member ComputeSquare upperLeft bottomRight =
                let sideLength =
                    computeIsoclesRightTriangleSideLength upperLeft bottomRight

                let upperRight =
                    { upperLeft with X = upperLeft.X + sideLength }

                let bottomLeft =
                    { upperLeft with Y = upperLeft.Y - sideLength }

                Rectangle.Create upperLeft upperRight bottomLeft bottomRight

            interface IDrawable with
                member this.Draw() : seq<Drawable> =
                    [ Drawable.Line(this.UpperLeft, this.UpperRight)
                      Drawable.Line(this.UpperRight, this.LowerRight)
                      Drawable.Line(this.LowerRight, this.LowerLeft)
                      Drawable.Line(this.LowerLeft, this.UpperLeft) ]

        [<RequireQualifiedAccess>]
        type Triangle =
            { First: Point
              Second: Point
              Third: Point }

            static member Create point1 point2 point3 =
                { First = point1
                  Second = point2
                  Third = point3 }

            interface IDrawable with
                member this.Draw() : seq<Drawable> =
                    [ Drawable.Line(this.First, this.Second)
                      Drawable.Line(this.Second, this.Third)
                      Drawable.Line(this.Third, this.First) ]

        [<RequireQualifiedAccess>]
        type Circle =
            | Regular of center: Point * radius: int

            interface IDrawable with
                member this.Draw() : seq<Drawable> =
                    match this with
                    | Circle.Regular (center, radius) ->
                        // NOTE[gastove|2021-07-05] So, this is interesting. We're going to compute the points on a
                        // circle using degrees. We're working, effectively, in motor steps — which is to say, integers
                        // (there's no meaningful thing that is a _partial_ motor step). But, we also can't just have
                        // every circle have 360 points. For tiny circles, this will be a meaningless level of detail.
                        // For large circles, this'll be a 360-sideded polygon.
                        //
                        // So, what we're gonna do is: generate 360 * a static scaling factor points, then clean
                        // them — make sure we're working only with whole numbers (probably by rounding). The
                        // plotter works best with *deltas*, not points, but delta management is the business of the
                        // renderer. But, to avoid passing in too much junk, we'll remove duplicate lines.
                        //
                        // Here's our basic point equation:
                        // x = radius * sin theta + center.x, y = radius * cos theta + center.y
                        //
                        // ...of course, it doesn't stop there. We need to compute the points on the circle, and we need
                        // to maintain order as we do so. *Then* we need to compute the deltas between those points.
                        // That's what we'll actually draw.

                        let points =
                            seq {
                                for i in 0 .. (360 * CircleScalingFactor) - 1 ->
                                    // Scale back down to an angle between 0 and 359.9...
                                    let angle = (float i) / (float CircleScalingFactor)

                                    let fradius = float radius

                                    let x =
                                        (fradius * (Math.Sin angle))
                                        |> Math.Round
                                        |> int
                                        |> (+) center.X

                                    let y =
                                        (fradius * (Math.Cos angle))
                                        |> Math.Round
                                        |> int
                                        |> (+) center.Y

                                    { X = x; Y = y }
                            }

                        let firstPoint = points |> Seq.head
                        let lastPoint = points |> Seq.last
                        let finalLine = Drawable.Line(firstPoint, lastPoint)


                        points
                        |> Seq.pairwise
                        |> Seq.map (fun (firstPoint, nextPoint) -> Drawable.Line(firstPoint, nextPoint))
                        |> Seq.distinct
                        |> Seq.append (seq { finalLine })

        /// This module contains utilities for generating random shapes. To do this,
        /// we model a `ShapeKind`, allowing code to specify a given shape without
        /// having to care about any of its parameters.

        type Shape =
            | Line
            | Triangle
            | Rectangle
            | Circle

            override this.ToString() =
                match this with
                | Line -> "line"
                | Triangle -> "triangle"
                | Rectangle -> "rectangle"
                | Circle -> "circle"

            static member FromString(input: string) =
                match input.ToLower() with
                | "line" -> Line |> Ok
                | "triangle" -> Triangle |> Ok
                | "circle" -> Circle |> Ok
                | "rectangle" -> Rectangle |> Ok
                | _ -> $"Failed to parse {input} as Shape" |> Error

            static member GetRandomKind(rng: System.Random) =
                let variants =
                    FSharp.Reflection.FSharpType.GetUnionCases(typeof<Shape>)
                    |> Array.map (fun caseInfo ->
                        match caseInfo.Name |> Shape.FromString with
                        | Ok t -> t
                        | Error e ->
                            failwith $"Somehow, we got an invalid type name, which should be impossible. Error is: {e}")

                let idx = rng.Next(variants.Length)

                variants.[idx]

            // TODO[gastove|2022-07-09] This should actually be random, el oh el.
            member this.GetRandom (rng: System.Random) pointXMax pointYMax : IDrawable =
                Rectangle.Create (Point.Create 0 0) (Point.Create 100 100) (Point.Create 100 0) (Point.Create 0 100)
    // match this with
    // | Line ->
    //     Shape.Line(
    //         beginning = Point.Random(rng, pointXMax, pointYMax),
    //         ending = Point.Random(rng, pointXMax, pointYMax)
    //     )
    // | Triangle ->
    //     Shape.Triangle(
    //         Point.Random(rng, pointXMax, pointYMax),
    //         Point.Random(rng, pointXMax, pointYMax),
    //         Point.Random(rng, pointXMax, pointYMax)
    //     )
    // | Rectangle ->
    //     let startingCorner = Point.Random(rng, pointXMax, pointYMax)
    //     let sideLength = System.Random().Next(1000)

    //     Shape.Rectangle(
    //         firstCorner = startingCorner,
    //         secondCorner = { startingCorner with X = startingCorner.X + sideLength },
    //         thirdCorner =
    //             { X = startingCorner.X + sideLength
    //               Y = startingCorner.Y + sideLength },
    //         fourthCorner = { startingCorner with Y = startingCorner.Y + sideLength }
    //     )
    // | Circle -> Shape.Circle(center = Point.Random(rng, pointXMax, pointYMax), radius = rng.Next(25, 501))
