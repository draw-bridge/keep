namespace DrawBridge.Plotter

open DrawBridge
open DrawBridge.Common.Types
open DrawBridge.Drawing.Geometry

module JsonRpc =

    module V1 =
#if FABLE_COMPILER
        open Thoth.Json
#else
        open Thoth.Json.Net
#endif

        type Method =
            | GetConfig
            | SetConfig
            | GetAttribute
            | SetAttribute
            | RaisePen
            | LowerPen
            | TogglePen
            // | SetPen
            | MovePen
            | Pause

            static member TryFromString =
                function
                | "v1.config.get" -> Some GetConfig
                | "v1.config.set" -> Some SetConfig
                | "v1.core.raise_pen" -> Some RaisePen
                | "v1.core.lower_pen" -> Some LowerPen
                | "v1.core.toggle_pen" -> Some TogglePen
                | "v1.core.move_pen" -> Some MovePen
                | _ -> None

            static member FromString s =
                match s |> Method.TryFromString with
                | Some s -> s |> Ok
                | None -> $"{s} is an invalid Method" |> Error

            override this.ToString() =
                match this with
                | RaisePen -> "v1.core.raise_pen"
                | LowerPen -> "v1.core.lower_pen"
                | TogglePen -> "v1.core.toggle_pen"
                | MovePen -> "v1.core.move_pen"
                // | SetPen -> "core.set_pen"
                | GetConfig -> "v1.config.get"
                | SetConfig -> "v1.config.set"
                | GetAttribute -> "v1.core.get_attribute"
                | SetAttribute -> "v1.core.set_attribute"
                | Pause -> "v1.core.pause"

        module Method =
            let encoder (method: Method) = method |> string |> Encode.string

            let decoder: Decoder<Method> =
                Decode.string
                |> Decode.andThen (fun maybeMethod ->
                    match maybeMethod |> Method.FromString with
                    | Ok m -> m |> Decode.succeed
                    | Error e -> e |> Decode.fail)

        // TODO[gastove|2021-02-20] Update this to support an `all` variant; will require support in `draw-bridge`.
        [<RequireQualifiedAccess>]
        type GettableConfig =
            | XTravel
            | YTravel
            | NativeResolutionFactor

            override this.ToString() =
                match this with
                | XTravel -> "x_travel"
                | YTravel -> "y_travel"
                | NativeResolutionFactor -> "native_resolution_factor"

            static member TryFromString =
                function
                | "x_travel" -> Some XTravel
                | "y_travel" -> Some YTravel
                | "native_resolution_factor" -> Some NativeResolutionFactor
                | _ -> None

            static member FromString s =
                match s |> GettableConfig.TryFromString with
                | Some gc -> gc |> Ok
                | None -> $"{s} is not a valid GettableConfig" |> Error

        module GettableConfig =

            let encoder (gc: GettableConfig) = gc |> string |> Encode.string

            let decoder: Decoder<GettableConfig> =
                Decode.string
                |> Decode.andThen (fun s ->
                    match s |> GettableConfig.FromString with
                    | Ok gc -> gc |> Decode.succeed
                    | Error e -> e |> Decode.fail)

        [<RequireQualifiedAccess>]
        type SettableConfig =
            | XTravel of float
            | YTravel of float

        // TODO[gastove|2022-07-16] I am *not* convinced the coding here makes
        // any damn sense. Check in on this, make sure the right answer is
        // documented somewhere good.
        module SettableConfig =
            let encoder (sc: SettableConfig) =
                match sc with
                | SettableConfig.XTravel xt ->
                    Encode.object [ "type", Encode.string "x_travel"; "x_travel", Encode.float xt ]
                | SettableConfig.YTravel yt ->
                    Encode.object [ "type", Encode.string "y_travel"; "y_travel", Encode.float yt ]

            let decoder: Decoder<SettableConfig> =
                Decode.field "type" Decode.string
                |> Decode.andThen (function
                    | "x_travel" ->
                        Decode.object (fun get -> SettableConfig.XTravel(get.Required.Field "x_travel" Decode.float))
                    | "y_travel" ->
                        Decode.object (fun get -> SettableConfig.YTravel(get.Required.Field "y_travel" Decode.float))
                    | invalid -> $"{invalid} is not a valid SettableConfig" |> Decode.fail)

        type QueryableAttribute =
            | FirmwareVersion
            | Nickname
            | MotorPosition

            override this.ToString() =
                match this with
                | FirmwareVersion -> "firmware_version"
                | Nickname -> "nickname"
                | MotorPosition -> "motor_position"

            static member FromString =
                function
                | "firmware_version" -> Ok FirmwareVersion
                | "nickname" -> Ok Nickname
                | "motor_position" -> Ok MotorPosition
                | other -> Error $"Invalid value {other}"

            static member TryFromString s =
                match QueryableAttribute.FromString s with
                | Ok v -> Some v
                | Error _ -> None

        module QueryableAttribute =
            let encoder (qa: QueryableAttribute) = qa |> string |> Encode.string

            let decoder: Decoder<QueryableAttribute> =
                Decode.string
                |> Decode.andThen (fun s ->
                    match s |> QueryableAttribute.FromString with
                    | Ok qa -> qa |> Decode.succeed
                    | Error e -> e |> Decode.fail)

        type SettableAttribute = Nickname of string

        module SettableAttribute =
            let encoder (sa: SettableAttribute) =
                match sa with
                | Nickname nick -> Encode.object [ "nickname", Encode.string nick ]

            let decoder: Decoder<SettableAttribute> =
                Decode.object (fun get -> Nickname(get.Required.Field "nickname" Decode.string))

        // TODO[gastove|2022-07-16] Write a `GetType` function or similar for
        // writing out the type name of this DU; use it for coders.
        type MethodParams =
            | PlotterOnly of Plotter
            | MovePenParams of plotter: Plotter * deltas: Delta list * duration: uint64 option
            | GetConfigParams of plotter: Plotter * configs: GettableConfig array
            | SetConfigParams of plotter: Plotter * configs: SettableConfig array
            | QueryPlotterAttributeParams of plotter: Plotter * attributes: QueryableAttribute array
            | SetPlotterAttributesParams of plotter: Plotter * attributes: SettableAttribute array
            // TODO[gastove|2022-07-16] Why does PauseParams use a Timespan while MovePenParams uses a uint64 option?
            | PauseParams of plotter: Plotter * duration: System.TimeSpan
        // | SetPenParams of plotter: Plotter * position: PenPosition


        module MethodParams =
            let encoder (mp: MethodParams) =
                match mp with
                | PlotterOnly plotter ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.core.plotter_only" |> Encode.string ]
                | MovePenParams(plotter, deltas, duration) ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.core.move_pen_params" |> Encode.string
                          "deltas", deltas |> List.map Delta.encoder |> Encode.list
                          "duration", duration |> Encode.option Encode.uint64 ]
                | GetConfigParams(plotter, configs) ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.config.get_params" |> Encode.string
                          "configs", configs |> Array.map GettableConfig.encoder |> Encode.array ]
                | SetConfigParams(plotter, configs) ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.config.set_params" |> Encode.string
                          "configs", configs |> Array.map SettableConfig.encoder |> Encode.array ]
                | QueryPlotterAttributeParams(plotter, attributes) ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.core.query_plotter_attr_params" |> Encode.string
                          "attributes", attributes |> Array.map QueryableAttribute.encoder |> Encode.array ]
                // TODO[gastove|2022-07-16] Fix pluralization of this variant -> Attribute vs Attributes
                | SetPlotterAttributesParams(plotter, attributes) ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.core.set_plotter_attr_params" |> Encode.string
                          "attributes", attributes |> Array.map SettableAttribute.encoder |> Encode.array ]
                | PauseParams(plotter, duration) ->
                    Encode.object
                        [ "plotter", plotter |> Plotter.encoder
                          "type", "v1.core.pause_params" |> Encode.string
                          "duration", duration |> Encode.timespan ]

            let decoder: Decoder<MethodParams> =
                Decode.field "type" Decode.string
                |> Decode.andThen (function
                    | "v1.core.plotter_only" ->
                        Decode.object (fun get -> PlotterOnly(get.Required.Field "plotter" Plotter.decoder))
                    | "v1.core.move_pen_params" ->
                        Decode.object (fun get ->
                            MovePenParams(
                                plotter = get.Required.Field "plotter" Plotter.decoder,
                                deltas = get.Required.Field "deltas" (Decode.list Delta.decoder),
                                duration = get.Optional.Field "duration" Decode.uint64
                            ))
                    | "v1.config.get_params" ->
                        Decode.object (fun get ->
                            GetConfigParams(
                                plotter = get.Required.Field "plotter" Plotter.decoder,
                                configs = get.Required.Field "configs" (Decode.array GettableConfig.decoder)
                            ))
                    | "v1.config.set_params" ->
                        Decode.object (fun get ->
                            SetConfigParams(
                                plotter = get.Required.Field "plotter" Plotter.decoder,
                                configs = get.Required.Field "configs" (Decode.array SettableConfig.decoder)
                            ))
                    | "v1.core.query_plotter_attr_params" ->
                        Decode.object (fun get ->
                            QueryPlotterAttributeParams(
                                plotter = get.Required.Field "plotter" Plotter.decoder,
                                attributes = get.Required.Field "attributes" (Decode.array QueryableAttribute.decoder)
                            ))
                    | "v1.core.set_plotter_attr_params" ->
                        Decode.object (fun get ->
                            SetPlotterAttributesParams(
                                plotter = get.Required.Field "plotter" Plotter.decoder,
                                attributes = get.Required.Field "attributes" (Decode.array SettableAttribute.decoder)
                            ))
                    | "v1.core.pause_params" ->
                        Decode.object (fun get ->
                            PauseParams(
                                plotter = get.Required.Field "plotter" Plotter.decoder,
                                duration = get.Required.Field "duration" Decode.timespan
                            ))
                    | invalid -> $"{invalid} cannot be decoded to a MethodParams" |> Decode.fail

                )

        type Request =
            { Id: int32 option
              Method: Method
              Params: MethodParams }

            static member Create id mthd paramz =
                { Id = id
                  Method = mthd
                  Params = paramz }

        module Request =
            let encoder (req: Request) =
                Encode.object
                    [ "jsonrpc", "2.0" |> Encode.string
                      "id", req.Id |> (Encode.option Encode.int)
                      "method", req.Method |> Method.encoder
                      "params", req.Params |> MethodParams.encoder ]

            let decoder: Decoder<Request> =
                Decode.object (fun get ->
                    { Id = get.Optional.Field "id" Decode.int
                      Method = get.Required.Field "method" Method.decoder
                      Params = get.Required.Field "params" MethodParams.decoder })

            let fromJsonString (s: string) = s |> Decode.fromString decoder

            let toJsonString (resp: Request) = resp |> encoder |> Encode.toString 0

        type ApiError =
            { Code: int
              Message: string }

            static member Create code message = { Code = code; Message = message }

            override this.ToString() = sprintf "%i: %s" this.Code this.Message

        module ApiError =
            let encoder (err: ApiError) =
                Encode.object [ "code", err.Code |> Encode.int; "message", err.Message |> Encode.string ]

            let decoder: Decoder<ApiError> =
                Decode.object (fun get ->
                    { Code = get.Required.Field "code" Decode.int
                      Message = get.Required.Field "message" Decode.string })

        type PlotterConfig =
            { XTravel: float
              YTravel: float
              NativeResolutionFactor: int }

        module PlotterConfig =
            let encoder (pc: PlotterConfig) =
                Encode.object
                    [ "x_travel", pc.XTravel |> Encode.float
                      "y_travel", pc.YTravel |> Encode.float
                      "native_resolution_factor", pc.NativeResolutionFactor |> Encode.int ]

            let decoder: Decoder<PlotterConfig> =
                Decode.object (fun get ->
                    { XTravel = get.Required.Field "x_travel" Decode.float
                      YTravel = get.Required.Field "y_travel" Decode.float
                      NativeResolutionFactor = get.Required.Field "native_resolution_factor" Decode.int })

        type PlotterMessage =
            | Unit
            | FirmwareVersion of major: int * minor: int * patch: int
            | Nickname of string
            | PenPosition of x: int * y: int
            | MotorStatus of executing: bool * xMoving: bool * yMoving: bool * motionQueueEmpty: bool
            | CurrentPenPosition of PenPosition

            override this.ToString() =
                match this with
                | Unit -> "Ok"
                | FirmwareVersion(major, minor, patch) -> $"v{major}.{minor}.{patch}"
                | Nickname(n) -> n
                | PenPosition(x, y) -> $"Pen is currently at (X:{x}, Y:{y})"
                | MotorStatus(executing, xMoving, yMoving, motionQueueEmpty) ->
                    $"Executing: {executing}; X-Motor Moving: {xMoving}; Y-Motor Moving: {yMoving}; Motion Queue Empty: {motionQueueEmpty}"
                | CurrentPenPosition(p) -> $"{p}"

        module PlotterMessage =
            let encoder (pm: PlotterMessage) =
                match pm with
                | Unit -> Encode.object [ "type", "unit" |> Encode.string ]
                | FirmwareVersion(major, minor, patch) ->
                    Encode.object
                        [ "type", "firmware_version" |> Encode.string
                          "major", major |> Encode.int
                          "minor", minor |> Encode.int
                          "patch", patch |> Encode.int ]
                | Nickname nick ->
                    Encode.object [ "type", "nickname" |> Encode.string; "nickname", nick |> Encode.string ]
                | PenPosition(x, y) ->
                    Encode.object
                        [ "type", "pen_position" |> Encode.string
                          "x", x |> Encode.int
                          "y", y |> Encode.int ]
                | MotorStatus(executing, xMoving, yMoving, motionQueueEmpty) ->
                    Encode.object
                        [ "type", "motor_status" |> Encode.string
                          "executing", executing |> Encode.bool
                          "x_moving", xMoving |> Encode.bool
                          "y_moving", yMoving |> Encode.bool
                          "motion_queue_empty", motionQueueEmpty |> Encode.bool ]
                | CurrentPenPosition penPos ->
                    Encode.object
                        [ "type", "pen_position" |> Encode.string
                          "pen_position", penPos |> PenPosition.encoder ]

            // TODO[gastove|2022-07-16] Finish this decoder
            let decoder: Decoder<PlotterMessage> =
                Decode.field "type" Decode.string
                |> Decode.andThen (function
                    | "unit" -> Unit |> Decode.succeed
                    | "firmware_version" ->
                        Decode.object (fun get ->
                            FirmwareVersion(
                                major = get.Required.Field "major" Decode.int,
                                minor = get.Required.Field "minor" Decode.int,
                                patch = get.Required.Field "patch" Decode.int
                            ))
                    | invalid -> $"{invalid} could not be decoded into a PlotterMessage" |> Decode.fail)



        [<RequireQualifiedAccess>]
        type ApiResult =
            | Ok of string array
            | Message of PlotterMessage
            | Config of PlotterConfig
            | Results of PlotterMessage array

        module ApiResult =
            let encoder (res: ApiResult) =
                match res with
                | ApiResult.Ok theStrings ->
                    Encode.object
                        [ "type", "ok" |> Encode.string
                          "data", theStrings |> Array.map Encode.string |> Encode.array ]
                | ApiResult.Message msg ->
                    Encode.object [ "type", "message" |> Encode.string; "data", msg |> PlotterMessage.encoder ]
                | ApiResult.Config pc ->
                    Encode.object [ "type", "config" |> Encode.string; "data", pc |> PlotterConfig.encoder ]
                | ApiResult.Results res ->
                    Encode.object
                        [ "type", "results" |> Encode.string
                          "data", res |> Array.map PlotterMessage.encoder |> Encode.array ]

            let decoder: Decoder<ApiResult> =
                Decode.field "type" Decode.string
                |> Decode.andThen (function
                    | "ok" ->
                        Decode.object (fun get -> ApiResult.Ok(get.Required.Field "data" (Decode.array Decode.string)))
                    | "message" ->
                        Decode.object (fun get -> ApiResult.Message(get.Required.Field "data" PlotterMessage.decoder))
                    | "config" ->
                        Decode.object (fun get -> ApiResult.Config(get.Required.Field "data" PlotterConfig.decoder))
                    | "results" ->
                        Decode.object (fun get ->
                            ApiResult.Results(get.Required.Field "data" (Decode.array PlotterMessage.decoder)))
                    | invalid -> $"{invalid} could not be decoded as an ApiResult" |> Decode.fail)

        type Response =
            { Id: int32 option
              Result: ApiResult option
              Error: ApiError option }

            static member Create id result error =
                { Id = id
                  Result = result
                  Error = error }

            member this.AsResult() =
                match this.Result, this.Error with
                | _, Some(e) -> Error(e)
                | None, None -> failwith "The api returned nether a result nor an error. This should be impossible."
                | Some(result), None -> Ok(result)

        module Response =
            let encoder (resp: Response) =
                Encode.object
                    [ "jsonrpc", "2.0" |> Encode.string
                      "id", resp.Id |> (Encode.option Encode.int)
                      "result", resp.Result |> (Encode.option ApiResult.encoder)
                      "error", resp.Error |> (Encode.option ApiError.encoder) ]

            let decoder: Decoder<Response> =
                Decode.object (fun get ->
                    { Id = get.Optional.Field "id" Decode.int
                      Result = get.Optional.Field "result" ApiResult.decoder
                      Error = get.Optional.Field "error" ApiError.decoder })

            let fromJsonString (s: string) = s |> Decode.fromString decoder

            let toJsonString (resp: Response) = resp |> encoder |> Encode.toString 0

        module Convert =

            let StringFromBytes (b: byte array) : string =
                b |> System.Text.Encoding.UTF8.GetString

            let BytesFromString (s: string) = s |> System.Text.Encoding.UTF8.GetBytes

            let JsonToBytes (jv: JsonValue) =
                jv |> Encode.toString 0 |> BytesFromString

        module Queries =

            let AllGettableConfigs =
                [| GettableConfig.XTravel
                   GettableConfig.YTravel
                   GettableConfig.NativeResolutionFactor |]

            let Config (plotter: Plotter) =
                let method = Method.GetConfig

                let paramz = MethodParams.GetConfigParams(plotter, AllGettableConfigs)

                let id = 0 |> Some

                Request.Create id method paramz
