namespace DrawBridge.Common

module Types =

#if FABLE_COMPILER
    open Thoth.Json
#else
    open Thoth.Json.Net
#endif

    [<RequireQualifiedAccess>]
    type DisposableResult<'T, 'E when 'T :> System.IDisposable> =
        | Ok of 'T
        | Error of 'E

        static member Return(x: 'T) : DisposableResult<'T, _> = Ok(x)

        static member Bind (binder: 'T -> 'U) (result: DisposableResult<'T, 'E>) : DisposableResult<'U, 'E> =
            match result with
            | Ok (d) -> Ok(binder d)
            | Error (e) -> Error(e)

        static member (>>=)(source, binder: 'T -> _) : DisposableResult<'U, 'E> = DisposableResult.Bind binder source

        static member BindIntoResult (binder: 'T -> _) (result: DisposableResult<'T, 'E>) : Result<_, 'E> =
            match result with
            | Ok (d) -> binder d
            | Error (e) -> Result.Error(e)

        interface System.IDisposable with
            member this.Dispose() =
                match this with
                | Ok (d) -> d.Dispose()
                | Error (_) -> ()

    type PenPosition =
        | Up
        | Down

        override this.ToString() =
            match this with
            | Up -> "up"
            | Down -> "down"

    module PenPosition =
        let encoder (penPos: PenPosition) = penPos |> string |> Encode.string

        let decoder: Decoder<PenPosition> =
            Decode.string
            |> Decode.andThen (function
                | "up" -> Decode.succeed Up
                | "down" -> Decode.succeed Down
                | invalid -> Decode.fail $"{invalid} is not a valid PenPosition")

    type Plotter =
        | AxiDrawV3
        | AxiDrawV3A3
        | AxiDrawV3XLX

        static member TryFromString(s: string) =
            match s.ToLower() with
            | "axidrawv3" -> Some(AxiDrawV3)
            | "axidrawv3a3" -> Some(AxiDrawV3A3)
            | "axidrawv3xlx" -> Some(AxiDrawV3XLX)
            | _ -> None

        static member FromString(s: string) =
            match s.ToLower() with
            | "axidrawv3" -> Ok AxiDrawV3
            | "axidrawv3a3" -> Ok AxiDrawV3A3
            | "axidrawv3xlx" -> Ok AxiDrawV3XLX
            | _ -> $"Cannot parse as plotter string {s}" |> Error

    module Plotter =
        let encoder (plotter: Plotter) = plotter |> string |> Encode.string

        let decoder: Decoder<Plotter> =
            Decode.string
            |> Decode.andThen (fun s ->
                match s |> Plotter.FromString with
                | Ok p -> p |> Decode.succeed
                | Error e -> e |> Decode.fail)
