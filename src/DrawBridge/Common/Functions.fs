namespace DrawBridge.Common

module Functions =
    /// Try to read an environment variable, returning
    let readEnvVar var =
        match System.Environment.GetEnvironmentVariable var with
        | null
        | "" -> None
        | s -> Some s
 