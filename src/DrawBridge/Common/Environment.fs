namespace DrawBridge.Common

module Environment = 
    [<Literal>]
    let DrawBridgeSocketEnvVar = "DRAWBRIDGE_SOCK"
 