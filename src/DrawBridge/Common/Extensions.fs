namespace DrawBridge.Common

module Extensions =

    module Result =

        let bindError (binder: 'ErrA -> Result<'ok, 'ErrB>) (maybeErr: Result<'ok, 'ErrA>) : Result<'ok, 'ErrB> =
            match maybeErr with
            | Error e -> e |> binder
            | Ok a -> a |> Ok
