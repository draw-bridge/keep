namespace DrawBridge

type Delta =
    { XDistance: int
      YDistance: int }

    static member Create x y = { XDistance = x; YDistance = y }

    member this.Zero = this.XDistance = 0 && this.YDistance = 0

type Point =
    { X: int32
      Y: int32 }

    static member Create x y = { X = x; Y = y }

    static member Default() = { X = 0; Y = 0 }

    static member Random(rng: System.Random, xLimit: int, yLimit: int) : Point =
        // Returns a number between min and max, inclusive/exclusive
        { X = rng.Next(1, xLimit + 1)
          Y = rng.Next(1, yLimit + 1) }

    member this.Delta(other: Point) =
        { XDistance = other.X - this.X
          YDistance = other.Y - this.Y }


[<RequireQualifiedAccess>]
type Drawable =
    | Point of Point
    | Line of (Point * Point)

/// An IRenderable is any $THING that can be converted into something we can
/// draw. An AxiDraw can only draw points or straight lines.
type IDrawable =
    abstract Draw: unit -> Drawable seq

type IRenderer<'Output> =
    abstract Render: IDrawable seq -> 'Output

type Dimensions =
    { Height: int
      Width: int }

    static member Create h w = { Height = h; Width = w }

    static member Zero = Dimensions.Create 0 0
