namespace DrawBridge

module Socket =
    open System.Net

    open DrawBridge.Common.Types

    let DefaultMessageTerminator = "\n\r"B

    type SocketError =
        | NoBytesSent
        | NoBytesReceived
        | NoConnection
        | SocketTimedOut
        | SocketException of Sockets.SocketException
        | Undefined of string

    type ISocket =
        inherit System.IDisposable
        abstract Send: byte array -> Result<int, SocketError>
        abstract Receive: flags: Sockets.SocketFlags -> Result<byte array, SocketError>
        abstract Open: unit -> DisposableResult<ISocket, SocketError>
        abstract IsOpen: unit -> bool

    type Socket =
        { Path: string
          Connection: Sockets.Socket option
          Terminator: byte array }

        interface ISocket with
            override this.Dispose() : unit =
                match this.Connection with
                | Some c -> c.Close()
                | None -> ()

            override this.Open() =
                let sock =
                    new Sockets.Socket(
                        Sockets.AddressFamily.Unix,
                        Sockets.SocketType.Stream,
                        Sockets.ProtocolType.Unspecified
                    )

                let domainSock = Sockets.UnixDomainSocketEndPoint(this.Path)

                try
                    sock.Connect(domainSock)
                    { this with Connection = Some sock } :> ISocket |> DisposableResult.Ok
                with
                | :? Sockets.SocketException as se -> DisposableResult.Error(SocketException(se))
                | exn -> DisposableResult.Error(Undefined(sprintf "%A" exn))

            override this.Send(bytes: byte array) : Result<int, SocketError> =
                match this.Connection with
                | Some c -> Ok(c.Send bytes)
                | None -> Error(NoConnection)

            override this.Receive(flags: Sockets.SocketFlags) : Result<byte array, SocketError> =
                match this.Connection with
                | Some c ->
                    // NOTE[gastove|2020-12-29] We'll wait, in all, five seconds
                    // for a response from DrawBridge. This is not a great
                    // solution; ideally, we'd just wait until either bytes
                    // became available or the connection was closed.
                    //
                    // Update: I can't seem to find a way to say, "block until
                    // >0 bytes are available or the conn closes." The docs seem
                    // to indicate checking Available is correct, which...
                    // surprises me. But, I should be able to get timeouts and
                    // retries to emulate the desired behavior reasonably
                    // closely.
                    let maxTries = 30000
                    let mutable currentTry = 0

                    while c.Available = 0 && currentTry < maxTries do
                        Async.Sleep 10 |> Async.RunSynchronously
                        currentTry <- currentTry + 1

                    let mutable result = Array.empty
                    let mutable received = 0

                    while c.Available > 0 do
                        let buffer = Array.zeroCreate c.Available
                        received <- received + c.Receive(buffer, 1, flags)
                        result <- Array.append result buffer

                    // NOTE[gastove|2020-12-31] So. I started seeing an
                    // error in which trying to read Available bytes would
                    // throw an error. I can't explain that; smells like a
                    // bug with System.Net.Socket on my OS. But, OK. We can
                    // read byte-at-a-time.
                    //
                    // Except that reading byte-at-a-time somehow *packs*
                    // the message with null bytes. The UTF8 string parser
                    // will read them very happiyly, but then Fleece chokes
                    // to death on a character you can't _see_, which is
                    // horrible.
                    //
                    // Fortunately, we can know that null byte wont actually
                    // be a character we care about, so we strip all of 'em
                    // out before returning.
                    result |> Array.filter (fun byte -> byte <> 0uy) |> Ok
                | None -> Error(NoConnection)

            override this.IsOpen() = this.Connection |> Option.isSome

        static member New(path: string) = { Socket.Default() with Path = path }

        static member Default() =
            { Path = ""
              Connection = None
              Terminator = DefaultMessageTerminator }

    let newSocket path = { Socket.Default() with Path = path }

    let openSocket (socket: ISocket) = socket.Open()

    /// A convenience wrapper around an ISocket, setting default flags for a Receive operation.
    let receive (socket: ISocket) =
        socket.Receive(Sockets.SocketFlags.None)

    /// A convenience wrapper around an ISocket, adding a message terminator before sending.
    let send (bytes: byte[]) (socket: ISocket) =
        [| bytes; DefaultMessageTerminator |] |> Array.concat |> socket.Send
