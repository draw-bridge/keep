module Reference

open Expecto

open DrawBridge
open DrawBridge.Common.Geometry
open DrawBridge.Common.Shapes
open DrawBridge.Common.Drawing.V1
open DrawBridge.Common.Types
open DrawBridge.Runner
open DrawBridge.Plotter.JsonRpc

[<Tests>]
let integrationTests =
    // TODO[gastove|2020-12-29] This needs to be managed by a Configuration record or similar
    let socketEnvVar = "DRAWBRIDGE_SOCK"
    // TODO[gastove|2020-12-29] Test that we get back the same ID we sent in for the drawing.
    let testDrawingId = 0

    let simpleTestDrawing =
        { Id = testDrawingId
          Plotter = AxiDrawV3A3
          Dimensions = { Height = 100; Width = 100 }
          Shapes = [ Line.CreateStraight (Point.Create 0 0) (Point.Create 100 100) ]
          Error = None }

    testSequenced <| testList
        "Testing the Run method against the Reference Plotter"
        [ testCase "I can run a very simple drawing and get correct results"
          <| fun _ ->
              // I really should be able to expect this, but right now it's too simplistic.
              // let expected =
              //     {Id = testDrawingId
              //      Error = None
              //      Result = Some "Ok" }

              let socketPath =
                  match System.Environment.GetEnvironmentVariable socketEnvVar with
                  | null
                  | "" -> failtestf "Could not read DrawBridge socket at env var %s" socketEnvVar
                  | p -> p

              let runner = Runner.New socketPath

              match runner.Run simpleTestDrawing with
              | Ok(results) -> Expect.equal results [V1.ApiResult.Ok([| "success" |])] "Everything should run successfully, especially while we aren't parsing responses"
              | Error(e) -> failtestf "Expected OK, but got error %A" e ]
