module App

open Feliz
open Feliz.UseDeferred
open Fable

open Fable.SimpleJson
open Fable.SimpleHttp

// open DrawBridge.Common.Drawing.V1

module Constants =
    let ApiBase = "http://localhost:5000"

type SvgDimensions = { Height: int; Width: int }

// TODO[gastove|2020-12-31] On the Axidraw, the wide axis is X. I need to be
// sure I'm representing everything correctly!

let printDrawing drawingId =
    async {
        let! response =
            Http.request $"{Constants.ApiBase}/api/print/{drawingId}"
            |> Http.method POST
            |> Http.send

        printfn "Post returned status %i" response.statusCode

        return ()
    }

let loadDrawing () =
    async {
        // TODO[gastove|2021-07-03] We need to not skip the status code, but I
        // don't have great error handling yet, so.
        let! (status, body) = Http.get $"{Constants.ApiBase}/api/generate"

        if status <> 200 then eprintf $"Failed to load drawing; {status}: {body}"

        return "DRAWING" //  body |> DrawBridge
    }

// let loadConfiguration (plotter: DrawBridge.Common.Types.Plotter) =
//     async {
//         let! (_, body) = Http.get $"{Constants.ApiBase}/api/config/get/{plotter}"

//         let config: Fleece.SystemTextJson.ParseResult<DrawBridge.Plotter.JsonRpc.V1.PlotterConfig> = Fleece.SystemTextJson.parseJson body

//         return config
//     }

// [<ReactComponent>]
// let SkillsOfAnArtist () =
//     let drawing, updateDrawing =
//         React.useState (Deferred.HasNotStartedYet)

//     let drawingLoader =
//         React.useDeferredCallback ((fun () -> loadDrawing ()), updateDrawing)

//     React.useEffectOnce (drawingLoader)

//     match drawing with
//     | Deferred.HasNotStartedYet -> Html.h1 "Started!"
//     | Deferred.InProgress -> Html.h1 "Keeeeep waiting"
//     | Deferred.Resolved (drawing) ->
//         match drawing with
//         | Some d ->
//             Html.div [ Svg.svg [ svg.height 1000
//                                  svg.width 1000
//                                  svg.viewBox (0, 0, d.Dimensions.Height, d.Dimensions.Width)
//                                  svg.xmlns "http://www.w3.org/2000/svg"
//                                  svg.children [] // (d.Shapes |> List.map Keep.UI.ToSvg.shapeToSvg)
//                                  svg.fill "white"
//                                  svg.fillOpacity 0
//                                  svg.stroke "black"
//                                  svg.strokeWidth 10 ]

//                        Html.button [ prop.onClick (fun _ -> drawingLoader ())
//                                      prop.text "Generate" ]

//                        Html.button [ prop.onClick (fun _ -> printDrawing d.Id |> Async.StartImmediate)
//                                      prop.text "Print!" ] ]

//         | None -> Html.h1 "The drawing failed to load. Or parse. Or both. Check the logs."

//     | Deferred.Failed (_) -> Html.h1 "fuckle dee dee"


let formatPlotterOption (plotter: string) =
    Html.option [ prop.value plotter
                  prop.id "plotter"
                  prop.children [ Html.text plotter ] ]


[<ReactComponent>]
let PlotterPicker () =

    let (currentPlotter, setCurrentPlotter) = React.useState ("")
    let (plotters, setPlotters) = React.useState ([])
    let dropdownRef = React.useInputRef ()
    let (isActive, setIsActive) = React.useState (false)

    let getCurrentPlotter () =
        async {
            let! (status, plotter) = Http.get $"{Constants.ApiBase}/api/plotter/get"

            match status with
            | 200 -> setCurrentPlotter plotter
            | _ -> printfn $"Status {status}, body is {plotter}"
        }

    let setBackendPlotter setTo =
        async {
            let! (status, msg) = Http.get $"{Constants.ApiBase}/api/plotter/set/{setTo}"

            match status with
            | 200 ->
                setCurrentPlotter setTo
                printfn $"{msg}"
            | _ -> printfn $"Setting current plotter failed with {status}: {msg}"
        }

    let loadPlotters () =
        async {
            let! (status, body) = Http.get $"{Constants.ApiBase}/api/plotter/list"

            match status with
            | 200 -> Json.parseAs<string list> body |> setPlotters
            | _ -> printfn $"Got non-200 status code {status}"
        }

    React.useEffectOnce (getCurrentPlotter >> Async.StartImmediate)
    React.useEffectOnce (loadPlotters >> Async.StartImmediate)

    let plotterButtons =
        plotters
        |> List.map
            (fun p ->
                Html.li [ prop.onClick (fun _ -> setBackendPlotter p |> Async.StartImmediate)
                          prop.text p ])

    Html.div [ prop.className "menu-container"
               prop.children [ Html.button [ prop.className "menu-trigger"
                                             prop.onClick (fun _ -> printfn "hi")
                                             prop.children [ Html.span [ Html.text currentPlotter ] ] ]

                               Html.ul [ prop.ref dropdownRef
                                         prop.className (
                                             if isActive then
                                                 "active"
                                             else
                                                 "inactive"
                                         )
                                         prop.children plotterButtons ] ] ]

[<ReactComponent>]
let Entrypoint () =

    Html.div [ // SkillsOfAnArtist()
               PlotterPicker() ]
