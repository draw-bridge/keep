namespace Keep.UI

module Drawing =
    type Dimensions = {Height: int; Width: int}
    // A mirror variant of the `Drawing` type in DrawBridge. But: this type uses
    // the SVG string data passed up by the server.
    type Drawing =
        { Id: int
          Svg: string
          Dimensions: Dimensions
          Plotter: DrawBridge.Common.Types.Plotter
          Error: string option }

module SerDe =

    open Fable.SimpleJson
    open DrawBridge.Common.Geometry

    open Drawing

    let parseNumber (obj: Json) =
        match obj with
        | JNumber n -> Some n
        | _ -> None

    let getPropFromObject prop (obj: Json) =
        match obj with
        | JObject dict -> Map.tryFind prop dict
        | _ -> None

    let getStringFromObject prop (obj: Json) : string option =
        getPropFromObject prop obj
        |> Option.bind (function
            | JString s -> Some s
            | _ -> None)

    let getNumberFromObject prop (obj: Json) : int option =
        getPropFromObject prop obj
        |> Option.bind (function
            | JNumber n -> Some(int n)
            | _ -> None)

    let tryParseJsonAstToPoint (obj: Json) =
        match obj with
        | JObject dict ->
            let x =
                Map.tryFind "x" dict |> Option.bind parseNumber

            let y =
                Map.tryFind "y" dict |> Option.bind parseNumber

            match x, y with
            | Some x, Some y -> { X = int x; Y = int y } |> Some
            | _ -> None
        | _ -> None

    let parsePointToPair (obj: Json) =
        obj
        |> tryParseJsonAstToPoint
        |> Option.map (fun p -> $"{p.X},{p.Y}")

    // let tryParseJsonAstToCircle ast =
    //     let cx = getNumberFromObject "center_x" ast
    //     let cy = getNumberFromObject "center_y" ast
    //     let r = getNumberFromObject "radius" ast

    //     match cx, cy, r with
    //     | Some cx, Some cy, Some r -> Circle({ X = cx; Y = cy }, r) |> Some
    //     | _ -> None

    // let tryParseJsonAstToRectangle ast =
    //     let firstCorner =
    //         ast
    //         |> getPropFromObject "first_corner"
    //         |> Option.bind tryParseJsonAstToPoint

    //     let secondCorner =
    //         ast
    //         |> getPropFromObject "second_corner"
    //         |> Option.bind tryParseJsonAstToPoint

    //     let thirdCorner =
    //         ast
    //         |> getPropFromObject "third_corner"
    //         |> Option.bind tryParseJsonAstToPoint

    //     let fourthCorner =
    //         ast
    //         |> getPropFromObject "fourth_corner"
    //         |> Option.bind tryParseJsonAstToPoint

    //     match firstCorner, secondCorner, thirdCorner, fourthCorner with
    //     | Some fc, Some sc, Some tc, Some fthc -> Rectangle(fc, sc, tc, fthc) |> Some

    //     | _ -> None

    // let tryParseJsonAstToShape ast =
    //     getStringFromObject "type" ast
    //     |> Option.bind
    //         (function
    //         | "circle" -> ast |> tryParseJsonAstToCircle
    //         | "rectangle" -> ast |> tryParseJsonAstToRectangle
    //         | _ -> None)

    // let tryDeserializeShape (input: string) =
    //     input
    //     |> SimpleJson.parse
    //     |> tryParseJsonAstToShape

    let tryDeserializeDimensions (dim: Json) =
        let h = getNumberFromObject "height" dim
        let w = getNumberFromObject "width" dim

        match h, w with
        | Some (height), Some (width) -> Some { Height = height; Width = width }
        | _ -> None

    let tryParseJsonAstToPlotter (ast: Json) =
        match ast with
        | JString plttr ->
            plttr
            |> DrawBridge.Common.Types.Plotter.FromString
            |> Some
        | _ -> None

    let tryParseSimpleJsonAstToDrawing (ast: Json) =
        let id, dimensions, shapes, plotter, error =
            match ast with
            | JObject map ->
                let shapes =
                    Map.tryFind "shapes" map
                    |> Option.bind (function
                        | JString shapes -> shapes |> Some
                        | _ -> None)

                let id = getNumberFromObject "id" ast

                let dimensions =
                    Map.tryFind "dimensions" map
                    |> Option.bind tryDeserializeDimensions

                let plotter =
                    Map.tryFind "plotter" map
                    |> Option.bind tryParseJsonAstToPlotter
                    |> Option.bind (function
                        | Ok p -> p |> Some
                        | Error _ -> None)

                let error =
                    Map.tryFind "error" map
                    |> Option.bind (function
                        | JString e -> e |> Some
                        | _ -> None)

                id, dimensions, shapes, plotter, error
            | _ -> None, None, None, None, None

        match id, dimensions, shapes, plotter with
        | Some i, Some d, Some s, Some p ->
            Some
                { Id = i
                  Svg = s
                  Dimensions = d
                  Plotter = p
                  Error = error }
        | _, _, _, _ -> None


    let tryDeserializeDrawing (body: string) =
        body
        |> SimpleJson.tryParse
        |> Option.bind tryParseSimpleJsonAstToDrawing
