namespace Keep.Messages.Tests

open Expecto

module SerDeTests =

    open Keep.Messages
    open Thoth.Json.Net

    module KeepResult =

        [<Tests>]
        let tests =
            testList
                "KeepResult JSON Round-Trip Tests"
                [ testCase "KeepResult.Message"
                  <| fun _ ->
                      let msg = "testing!" |> KeepResult.Message
                      let json = msg |> KeepResult.encoder |> string
                      let gotBack = json |> Decode.fromString KeepResult.decoder

                      Expect.equal gotBack (Ok(msg)) "We should be able to round-trip a Message"

                  testCase "KeepResult.Drawing"
                  <| fun _ ->
                      let id = 1
                      let name = "test"
                      let drawing = KeepResult.Drawing(id = id, name = name)

                      let roundTripped =
                          drawing
                          |> KeepResult.encoder
                          |> string
                          |> Decode.fromString KeepResult.decoder

                      Expect.equal roundTripped (Ok(drawing)) "We should be able to round-trip a Drawing" ]

    module KeepResponse =
        [<Tests>]
        let tests =
            testList
                "KeepResponse round-trip JSON Tests"
                [ testCase "Successful Message"
                  <| fun _ ->
                      let msg = "testing!" |> KeepResult.Message
                      let json = msg |> Ok |> KeepResponse.encoder |> string

                      let gotBack = json |> Decode.fromString KeepResponse.decoder

                      match gotBack with
                      | Ok(kr) -> Expect.equal kr (Ok(msg)) "We should be able to round-trip a Message"
                      | Error(e) -> failtest $"JSON decoding failed with {e}"

                  ]
