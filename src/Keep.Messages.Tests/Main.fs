﻿namespace Keep.Common.Tests

open Expecto

module Main =
    [<EntryPoint>] 
    let main argv =
        Tests.runTestsInAssemblyWithCLIArgs [] argv
