/// This integration module does "round-trip" testing of DrawBridge against the
/// published API spec. This means it needs access to said API Spec! Typically,
/// this means running these tests inside a container, for which this project
/// includes a Dockerfile.
module DrawBridge.ApiSpecIntegration

open Expecto

module ApiSpec =

    open System

    open FSharpPlus
    open Thoth.Json.Net

    open DrawBridge.Plotter.JsonRpc.V1

    let jsonRoot = "/json"
    let requests = "requests"
    let responses = "responses"

    type MessageSpec =
        { Cmd: Method
          Request: string
          Response: string }

        static member New name request response =
            { Cmd = name
              Request = request
              Response = response }

        static member ReadFromPaths cmd (paths: string list) : MessageSpec =
            let isSpecType (theType: string) (path: string) = path.Contains(theType)
            let read path = IO.File.ReadAllText path

            let request =
                paths |> List.find (isSpecType "requests") |> read

            let response =
                paths
                |> List.find (isSpecType "responses")
                |> read

            MessageSpec.New cmd request response

    let loadSpecFilePaths (path: string) : string array = IO.Directory.GetFiles(path)

    let parsePairs (paths: string array) : (Method * string) list =
        paths
        |> Array.choose (fun path ->
            IO.Path.GetFileNameWithoutExtension(path)
            |> Method.TryFromString
            |> Option.map (fun cmd -> cmd, path))
        |> List.ofArray

    let loadMessageSpecs () : MessageSpec list =
        let requestsPath = IO.Path.Combine [| jsonRoot; requests |]

        let responsesPath =
            IO.Path.Combine [| jsonRoot
                               responses |]

        let rawRequests =
            requestsPath |> loadSpecFilePaths |> parsePairs

        let rawResponses =
            responsesPath |> loadSpecFilePaths |> parsePairs

        [ rawRequests; rawResponses ]
        |> List.concat
        |> List.groupBy fst
        |> List.map (fun (specName, pathsList) ->
            let paths = pathsList |> List.map snd
            MessageSpec.ReadFromPaths specName paths)


    [<Tests>]
    let serdeTests =

        testList
            "Serialization Round-Tripping"
            [ testCase "Can I deserialize JSON-RPC Request/Response Pairs?"
              <| fun _ ->
                  for spec in loadMessageSpecs () do
                      let request: Request =
                          spec.Request
                          |> Decode.fromString Request.decoder
                          |> Result.get

                      let response: Response =
                          spec.Response
                          |> Decode.fromString Response.decoder
                          |> Result.get

                      Expect.equal
                          spec.Cmd
                          request.Method
                          "We should parse the correct command on the Request.Method field"

                      Expect.equal request.Id response.Id "We should have the same ID on both objects"

              testCase "If I deserialize, serialize, and deserialize again, I should have the same object"
              <| fun _ ->
                  for spec in loadMessageSpecs () do
                      let request: Request =
                          spec.Request
                          |> Decode.fromString Request.decoder
                          |> Result.get

                      let roundTrippedRequest: Request =
                          request
                          |> Request.toJsonString
                          |> Request.fromJsonString
                          |> Result.get

                      let response: Response =
                          spec.Response
                          |> Response.fromJsonString
                          |> Result.get

                      let rountTrippedResponse =
                          response
                          |> Response.toJsonString
                          |> Response.fromJsonString
                          |> Result.get

                      Expect.equal roundTrippedRequest request "The request should match its source"
                      Expect.equal rountTrippedResponse response "The response should match its source" ]


[<EntryPoint>]
let main argv =
    Tests.runTestsInAssembly defaultConfig argv
