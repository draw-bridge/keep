module RenderingTests

open Expecto

open DrawBridge.Plotter
open DrawBridge.Common.Types
open DrawBridge.Drawing

let ThePlotter = AxiDrawV3A3
let PlotterOnlyCmd = ThePlotter |> JsonRpc.V1.PlotterOnly

module InternalTests =

    open DrawBridge.Plotter.JsonRpc.V1
    open DrawBridge.Rendering.Internal

    [<Tests>]
    let pointDirectionTests =

        let testSpecs =
            [ {| Name = "Directions to Point from Origin"
                 InitialState = PlotterState.Create(Point.Create 0 0) Array.empty
                 FromPoint = Point.Create 10 10
                 EndingState =
                  PlotterState.Create
                      (Point.Create 10 10)
                      [| MovePen, MovePenParams(ThePlotter, [ Delta.Create 10 10 ], None)
                         LowerPen, PlotterOnlyCmd
                         RaisePen, PlotterOnlyCmd |]
                 Message = "We should get back reasonable simple motion." |}
              {| Name = "Motion between identical points is a Noop"
                 InitialState = PlotterState.Create(Point.Create 0 0) Array.empty
                 FromPoint = Point.Create 0 0
                 EndingState = PlotterState.Create(Point.Create 0 0) Array.empty
                 Message = "Motion from the same point we start at should be a noop" |}

              ]

        testSpecs
        |> List.map (fun spec ->
            testCase spec.Name
            <| fun _ ->
                let actual =
                    directionsForPoint spec.InitialState ThePlotter spec.FromPoint

                Expect.equal actual spec.EndingState spec.Message)
        |> testList "Rendering.PlotterRenderer.Internal.directionsForPoint"

    [<Tests>]
    let lineDirectionTests =
        let testSpecs =
            [ {| Name = "We can compute correct directions for drawing a basic line with same origin as starting state"
                 InitialState = PlotterState.Create(Point.Create 0 0) Array.empty
                 DrawLine = Point.Create 0 0, Point.Create 127 127
                 EndingState =
                  PlotterState.Create
                      (Point.Create 127 127)
                      [| MovePen, MovePenParams(ThePlotter, [ (Delta.Create 0 0) ], None)
                         LowerPen, PlotterOnlyCmd
                         MovePen, MovePenParams(ThePlotter, [ (Delta.Create 127 127) ], None)
                         RaisePen, PlotterOnlyCmd |]
                 Message = "We should be able to draw a straight line" |}
              {| Name =
                  "We can compute correct directions for drawing a basic line with different origin from starting state"
                 InitialState = PlotterState.Create(Point.Create 0 0) Array.empty
                 DrawLine = Point.Create 50 50, Point.Create 100 100
                 EndingState =
                  PlotterState.Create
                      (Point.Create 100 100)
                      [| MovePen, MovePenParams(ThePlotter, [ (Delta.Create 50 50) ], None)
                         LowerPen, PlotterOnlyCmd
                         MovePen, MovePenParams(ThePlotter, [ (Delta.Create 50 50) ], None)
                         RaisePen, PlotterOnlyCmd |]
                 Message = "We should be able to draw a straight line" |} ]

        testSpecs
        |> List.map (fun spec ->
            testCase spec.Name
            <| fun _ ->
                let actual =
                    directionsForLine spec.InitialState ThePlotter spec.DrawLine

                Expect.equal actual spec.EndingState spec.Message)
        |> testList "Rendering.PlotterRenderer.Internal.directionsForLine"
