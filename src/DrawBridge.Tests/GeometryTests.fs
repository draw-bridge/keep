module GeometryTests

open Expecto

open DrawBridge.Drawing
open DrawBridge.Drawing.Geometry

[<Tests>]
let commonTests =
    testList
        "Testing types and methods in DrawBridge.Common.Geometry"
        [ testCase "Can I correctly compute the delta between two points?"
          <| fun _ ->
              let firstPoint = { X = 0; Y = 0 }
              let secondPoint = { X = 10; Y = 10 }

              let firstGot = firstPoint.Delta secondPoint
              let firstExpected = Delta.Create 10 10

              let secondGot = secondPoint.Delta firstPoint
              let secondExpected = Delta.Create -10 -10

              Expect.equal
                  firstGot
                  firstExpected
                  "We should be able to compute deltas correctly in the positive direction"

              Expect.equal
                  secondGot
                  secondExpected
                  "We should be able to compute deltas correctly in the negative direction"

          testCase "Can I correctly compute the linear distance between two points horizontally?"
          <| fun _ ->
              let firstPoint = Point.Create 0 0
              let secondPoint = Point.Create 10 0

              let expected = 10
              let actual = distanceBetween firstPoint secondPoint

              Expect.equal actual expected "We should be able to compute a horizontal distance accurately"

          testCase "Can I correctly compute the linear distance between two points vertically?"
          <| fun _ ->
              let firstPoint = Point.Create 0 0
              let secondPoint = Point.Create 0 10

              let expected = 10
              let actual = distanceBetween firstPoint secondPoint

              Expect.equal actual expected "We should be able to compute a horizontal distance accurately"

          testCase "Can I compute the side length of a 45°x45°x90° right triangle?"
          <| fun _ ->
              let firstPoint = Point.Create 0 0
              let secondPoint = Point.Create 141 0 //sqrt(2) * 100

              let expected = 100

              let actual =
                  computeIsoclesRightTriangleSideLength firstPoint secondPoint

              Expect.equal actual expected "This should be approximately correct" ]
