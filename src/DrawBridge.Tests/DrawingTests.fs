module DrawingTests

open Expecto

open DrawBridge
open DrawBridge.Drawing
open DrawBridge.Common.Types
open DrawBridge.Plotter.JsonRpc.V1

[<Tests>]
let drawingTests =
    let testHeight = 100
    let testWidth = 100

    let testDimensions =
        { Height = testHeight
          Width = testWidth }

    let makeTestDrawingWithShapes shapes =
        { Id = 0
          Dimensions = testDimensions
          Shapes = shapes
          Plotter = AxiDrawV3
          Error = None }

    let makePenParams deltas = MovePenParams(AxiDrawV3, deltas, None)

    let plotterRenderer = Rendering.PlotterRenderer.Create 1 Plotter.AxiDrawV3

    // NOTE[gastove|2021-01-02] Remember that every Drawing starts with raise
    // pen and ends with a home move.
    testList
        "Testing the Drawing record and its methods"
        [
          // TODO: okay, yes, it is actually terrible that I'm not checking
          // commands any more. This isn't great. Need to figure out where
          // testing should live in the new Rendering paradigm.

          // testCase "Can we render the correct Commands, with the correct Deltas, for drawing a line?"
          // <| fun _ ->
          //     let drawing =
          //         makeTestDrawingWithShapes [ Line.CreateStraight (Point.Create 0 0) (Point.Create 100 100) ]

          //     let commands = drawing.GetCommands(plotterRenderer.Render)

          //     let expected =
          //         [  // Not sure why this was here, need to verify it's correct that it's missing.
          //           // RaisePen, PlotterOnly drawing.Plotter
          //           MovePen, makePenParams [ Delta.Create 0 0 ]
          //           LowerPen, PlotterOnly drawing.Plotter
          //           MovePen, makePenParams [ Delta.Create 100 100 ]
          //           RaisePen, PlotterOnly drawing.Plotter
          //           MovePen, makePenParams [ Delta.Create -100 -100 ] ]

          //     Expect.equal commands expected "We should compute the correct motion"

          // testCase "Can we render the correct Commands, with the correct Deltas, for drawing a rectangle?"
          // <| fun _ ->
          //     let rect =
          //         Rectangle.Exact(Point.Create 25 25, Point.Create 75 25, Point.Create 75 75, Point.Create 25 75)

          //     let drawing = makeTestDrawingWithShapes [ rect ]

          //     let expected =
          //         [ // Not sure why this was here, need to verify it's correct that it's missing.
          //           // RaisePen, PlotterOnly drawing.Plotter
          //           MovePen, makePenParams [ Delta.Create 25 25 ]
          //           LowerPen, PlotterOnly drawing.Plotter
          //           MovePen,
          //           makePenParams [ Delta.Create 50 0
          //                           Delta.Create 0 50
          //                           Delta.Create -50 0
          //                           Delta.Create 0 -50 ]

          //           RaisePen, PlotterOnly drawing.Plotter
          //           MovePen, makePenParams [ Delta.Create -25 -25 ] ]

          //     let got = drawing.GetCommands(plotterRenderer.Render)

          //     Expect.equal got expected "We should compute the correct motion"

          ]
