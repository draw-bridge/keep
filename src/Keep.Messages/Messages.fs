namespace Keep.Messages

#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// NOTE[gastove|2023-07-22] The things I do to not use magic strings.
module private KeepErrorNames =
    [<Literal>]
    let SerdeErrorName = "SerdeError"

    [<Literal>]
    let SocketErrorName = "SocketError"

    [<Literal>]
    let PlotterErrorName = "PlotterError"

    [<Literal>]
    let HttpErrorName = "HttpError"

    [<Literal>]
    let RunnerErrorName = "RunnerError"

    [<Literal>]
    let ServerErrorName = "ServerError"

/// When we comminucate with drawbridge, a lot can go wrong. The socket
/// may error, or we may fail to decode the JSON we get back, or we may
/// decode the json and find it contains an error.
///
/// This type should help us roll everything else up into something we can
/// effectively display to the end user. Each variant takes a `string` message,
/// relying on the type itself for extra semantic meaning.
[<RequireQualifiedAccess>]
type KeepError =
    | SerdeError of string
    | SocketError of string
    | PlotterError of string
    | HttpError of string
    | RunnerError of string // Runner.RunnerError
    | ServerError of string

    member private this.ErrorName =
        match this with
        | SerdeError _ -> KeepErrorNames.SerdeErrorName
        | SocketError(_) -> KeepErrorNames.SocketErrorName
        | PlotterError(_) -> KeepErrorNames.PlotterErrorName
        | HttpError(_) -> KeepErrorNames.HttpErrorName
        | RunnerError(_) -> KeepErrorNames.RunnerErrorName
        | ServerError(_) -> KeepErrorNames.ServerErrorName

    member private this.ErrorMessage =
        match this with
        | SerdeError msg -> msg
        | SocketError msg -> msg
        | PlotterError msg -> msg
        | HttpError msg -> msg
        | RunnerError msg -> msg
        | ServerError msg -> msg

    override this.ToString() =
        $"{this.ErrorName}: {this.ErrorMessage}"

    static member FromString(s: string) =
        let parts = s.Split(':', System.StringSplitOptions.TrimEntries)

        match parts with
        | [| KeepErrorNames.SerdeErrorName; msg |] -> msg |> SerdeError
        | [| KeepErrorNames.HttpErrorName; msg |] -> msg |> HttpError
        | [| KeepErrorNames.PlotterErrorName; msg |] -> msg |> PlotterError
        | [| KeepErrorNames.RunnerErrorName; msg |] -> msg |> RunnerError
        | [| KeepErrorNames.ServerErrorName; msg |] -> msg |> ServerError
        | [| KeepErrorNames.SocketErrorName; msg |] -> msg |> SocketError
        | _ -> failwithf "Failed to parse %s into a KeepError" s

    static member TryFromString(s: string) =
        try
            s |> KeepError.FromString |> Ok
        with Failure(msg) ->
            msg |> Error

module KeepError =
    let encoder (ke: KeepError) = ke |> string |> Encode.string

    let decoder: Decoder<KeepError> =
        Decode.string
        |> Decode.andThen (fun maybeKeepError ->
            match maybeKeepError |> KeepError.TryFromString with
            | Ok ke -> ke |> Decode.succeed
            | Error e -> e |> Decode.fail)


// TODO[gastove|2023-07-23] This type might be correct, but it isn't going to
// work as written. Not at all. Can't decode it once encoded as written.
type KeepResult =
    | Message of string
    | Drawing of id: int * name: string

    member this.ResultType =
        match this with
        | KeepResult.Message _ -> "message"
        | KeepResult.Drawing(_) -> "drawing"

module KeepResult =
    let encoder (kr: KeepResult) =
        let rt = [ "result_type", kr.ResultType |> Encode.string ]

        let rest =
            match kr with
            | Message s -> [ "message", s |> Encode.string ]
            | Drawing(id = id; name = name) ->
                [ "drawing", Encode.object [ "id", id |> Encode.int; "name", name |> Encode.string ] ]

        [ rt; rest ] |> List.concat |> Encode.object

    let private decodeMessage: Decoder<KeepResult> =
        Decode.string
        |> Decode.andThen (fun s -> s |> KeepResult.Message |> Decode.succeed)

    let private decodeDrawing: Decoder<KeepResult> =
        Decode.object (fun get ->
            let drawingId = get.Required.Field "id" Decode.int
            let drawingName = get.Required.Field "name" Decode.string

            KeepResult.Drawing(id = drawingId, name = drawingName))

    let decoder: Decoder<KeepResult> =
        Decode.field "result_type" Decode.string
        |> Decode.andThen (fun resultType ->
            match resultType with
            | "message" -> Decode.field "message" decodeMessage
            | "drawing" -> Decode.field "drawing" decodeDrawing
            | invalid -> $"Cannot parse KeepResult from {invalid}" |> Decode.fail)

type KeepResponse = Result<KeepResult, KeepError>

module KeepResponse =

    let encoder (kr: KeepResponse) =
        match kr with
        | Ok data -> Encode.object [ "data", data |> KeepResult.encoder; "error", "none" |> Encode.string ]
        | Error e -> Encode.object [ "data", "none" |> Encode.string; "error", e |> KeepError.encoder ]

    let decoder: Decoder<KeepResponse> =
        Decode.object (fun get ->
            match get.Optional.Field "error" Decode.string with
            | Some("none")
            | None -> get.Required.Field "data" KeepResult.decoder |> Ok
            | Some(e) -> e |> KeepError.FromString |> Error)
