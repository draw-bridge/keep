// namespace Keep.Test.Common

// NOTE[gastove|2022-07-10] It's not clear to me that *any* of this is used. Get
// everything building, the tests passing, and re-visit.

// module Constants =

//     open DrawBridge.Common.Shapes

//     let svgFile = "sample.svg.xml"

//     let svgFileShapes =
//         [ Circle
//             ({ X = 50
//                Y = 50 }, 25)
//           Line
//               ({ X = 350
//                  Y = 350 },
//                { X = 450
//                  Y = 350 })
//           Line
//               ({ X = 450
//                  Y = 350 },
//                { X = 450
//                  Y = 450 })
//           Line
//               ({ X = 450
//                  Y = 450 },
//                { X = 350
//                  Y = 450 })
//           Line
//               ({ X = 350
//                  Y = 450 },
//                { X = 350
//                  Y = 350 }) ]

// module XmlUtils =
//     open System
//     open System.Xml

//     let xmlTestDoc (name: string) (attrs: Map<string, string>): XmlElement =
//         let doc = XmlDocument()
//         let elem = doc.CreateElement name

//         for attr in attrs do
//             elem.SetAttribute(attr.Key, attr.Value)

//         elem

//     let readResource resource =
//         let assembly = System.Reflection.Assembly.GetExecutingAssembly()
//         let path = sprintf "%s.resources.%s" (assembly.GetName().Name) resource

//         let stream = assembly.GetManifestResourceStream(path)
//         let reader = new IO.StreamReader(stream)

//         reader.ReadToEnd()

//     let loadXmlDoc (s: string): XmlDocument =
//         let doc = XmlDocument()
//         doc.LoadXml(s)
//         doc
