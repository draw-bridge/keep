namespace Keep.Svg

open DrawBridge.Geometry.Shapes

module Test =

    open Expecto
    open Svg
    open Svg.Types

    open Keep.Test.Common

    [<Tests>]
    let utilTests =
        testList "Svg Utilities Tests"
            [ testCase "I should be able to get an attribute from an XmlElement" <| fun _ ->
                let testAttr1 = "testOne"
                let testAttr2 = "testTwo"
                let attr1Val = "valOne"
                let attr2Val = "valTwo"

                let attrs =
                    [ testAttr1, attr1Val
                      testAttr2, attr2Val ]
                    |> Map.ofList

                let elem = XmlUtils.xmlTestDoc "test" attrs

                let expectedOne = Some(attr1Val)
                let expectedTwo = Some(attr2Val)

                let gotOne = Xml.Util.getAttrFromNode testAttr1 elem
                let gotTwo = Xml.Util.getAttrFromNode testAttr2 elem

                Expect.equal expectedOne gotOne "I should get back valOne"
                Expect.equal expectedTwo gotTwo "I should get back valTwo"

              testCase "I should be able to construct a folding function around a node" <| fun _ ->
                  let testAttr1 = "testOne"
                  let testAttr2 = "testTwo"
                  let attr1Val = "valOne"
                  let attr2Val = "valTwo"

                  let attrs =
                      [ testAttr1, attr1Val
                        testAttr2, attr2Val ]
                      |> Map.ofList

                  let elem = XmlUtils.xmlTestDoc "test" attrs
                  let folder = Svg.Xml.Util.makeFolder elem
                  let acc: Map<string, string> = Map.empty

                  let got = [ testAttr1; testAttr2 ] |> List.fold folder acc

                  Expect.isTrue (got.ContainsKey(testAttr1)) "Key one should be present"
                  Expect.isTrue (got.ContainsKey(testAttr2)) "Key two should be present"
                  Expect.equal (got.[testAttr1]) attr1Val "Value 1 should be at key 1"
                  Expect.equal (got.[testAttr2]) attr2Val "Value 2 should be at key 2" ]

    [<Tests>]
    let xmlTests =
        testList "Svg.Xml Module Tests"
            [ testCase "I should be able to construct a circle from an XmlElement" <| fun _ ->
                let testX = 5
                let testY = 5
                let testRadius = 5

                let expected =
                    Circle
                        ({ X = testX
                           Y = testY }, testRadius)

                let attrs =
                    [ "cx", string testX
                      "cy", string testY
                      "r", string testRadius ]
                    |> Map.ofList

                let elem = XmlUtils.xmlTestDoc "circle" attrs
                let gotOpt = Svg.Xml.nodeToCircle elem

                Expect.isSome gotOpt "should be none"
                let got = Option.get gotOpt
                Expect.equal expected got "I should get back a matching circle"

                let nextGot = Svg.Xml.xmlToShape elem |> Option.get
                Expect.equal got nextGot "I should get back the same thing from this entrypoint"

              testCase "I should be able to parse an XML string to a Shape list" <| fun _ ->
                  let xmlString = XmlUtils.readResource Constants.svgFile

                  let doc = XmlUtils.loadXmlDoc xmlString

                  let got = Svg.Xml.docToShapeList doc.FirstChild

                  Expect.isNonEmpty got "it should not return an empty list"
                  Expect.equal got Constants.svgFileShapes "We should get the shapes expected"

 ]
