namespace Keep.Bridge

open Expecto

// open Keep
// open Keep.Common

module Test =
    [<Tests>]
    let placeHolder =
        testList "This doesn't do a thing"
            [ testCase "This is a place holder" <| fun _ -> Expect.equal 1 1 "Is reality real" ]

// There's a non-zero chance this should exist, but I honestly can't remember if I'm doing XML or JSON.
//
// [<Tests>]
// let bridgeTests =
//     testList "Bridge Module Tests"
//         [ testCase "I should be able to parse a Shape list into an InstructionSet" <| fun _ ->
//             let xmlString = XmlUtils.readResource Constants.svgFile
//             let doc = XmlUtils.loadXmlDoc xmlString
//             let shapes = Svg.Xml.docToShapeList doc.FirstChild

//             let got = Svg.Xml.shapesToInstructions shapes

//             Expect.isNonEmpty got "it should not return an empty list"

//           testCase "I should be able to interpret an XmlDocument into an InstructionSet" <| fun _ ->
//               let instructions =
//                   XmlUtils.readResource Constants.svgFile
//                   |> XmlUtils.loadXmlDoc
//                   |> Svg.Xml.interpret

//               Expect.isNonEmpty instructions "it should not be empty" ]
