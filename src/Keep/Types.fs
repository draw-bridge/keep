namespace Keep

module Types =

#if FABLE_COMPILER
    open Thoth.Json
#else
    open Thoth.Json.Net
#endif

 
    open DrawBridge
    open DrawBridge.Common.Drawing.V1

    /// When we comminucate with drawbridge, a lot can go wrong. The socket
    /// may error, or we may fail to decode the JSON we get back, or we may
    /// decode the json and find it contains an error.
    ///
    /// This type should help us roll everything else up into something we
    /// can effectively display to the end user.
    [<RequireQualifiedAccess>]
    type KeepError =
        | SerdeError of string
        | SocketError of string
        | PlotterError of string
        | HttpError of string
        | RunnerError of Runner.RunnerError
        | ServerError of string

        override this.ToString() =
            match this with
            | SerdeError (s) -> sprintf "SerDe Error: %s" s
            | SocketError (s) -> sprintf "Socket Error: %s" s
            | PlotterError (s) -> sprintf "Plotter Error: %s" s
            | HttpError (s) -> sprintf "Http Error: %s" s
            | RunnerError (re) -> $"Runner Error: {re}"
            | ServerError (se) -> $"Server Error: {se}"

        static member OfDecodeError(e: string) = e |> SerdeError

        static member OfSocketError(e: DrawBridge.Socket.SocketError) = string e |> SocketError

        static member OfRunnerError(re: Runner.RunnerError) = re |> RunnerError

        static member BindDecodeError(e: string) = e |> KeepError.OfDecodeError |> Error

        static member BindSocketError(e: DrawBridge.Socket.SocketError) = e |> KeepError.OfSocketError |> Error

    module KeepError =
        let encoder (ke: KeepError) = ke |> string |> Encode.string

    type KeepResult =
        | Message of string
        | ApiResult of Plotter.JsonRpc.V1.ApiResult
        | DrawingResponse of Drawing
        | Svg of string 

    module KeepResult =
        let encoder (kr: KeepResult) =
            match kr with
            | Message s -> s |> Encode.string
            | ApiResult ap -> ap |> Plotter.JsonRpc.V1.ApiResult.encoder
            | DrawingResponse _d -> "we can't encode a drawing" |> Encode.string
            | Svg svg -> svg |> Encode.string            

    type KeepResponse = Result<KeepResult, KeepError>
    
    module KeepResponse =

        let encoder (kr: Result<KeepResult, KeepError>) =
            match kr with
                | Ok data -> 
                    Encode.object [ "data", data |> KeepResult.encoder
                                    "error", "none" |> Encode.string ]
                | Error e ->
                    Encode.object [ "data", "none" |> Encode.string
                                    "error", e |> KeepError.encoder ]


