﻿open System
open System.Text

open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection

open Giraffe

#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

open Thoth.Json.Giraffe

// open DrawBridge
// open DrawBridge.Drawing

open Keep.Messages

// TODO[gastove|2020-12-31] On the Axidraw, the wide axis is X. I need to be
// sure I'm representing everything correctly!

// let mutable drawings: Drawing list = List.empty

let defaultPlotter =
    DrawBridge.Common.Types.Plotter.AxiDrawV3A3

let mutable configuredPlotter = defaultPlotter

// let recordDrawing drawing =
//     drawings <- List.append drawings [ drawing ]

// let getDrawingById id =
//     match List.tryItem id drawings with
//     | Some drawing -> drawing |> Ok
//     | None ->
//         $"No drawing found with id: {id}"
//         |> KeepError.ServerError
//         |> Error

// TODO[gastove|2020-07-09] Drawing dimensions need to be parameterized; we need
// to set them first from plotter query, then from user settings.
// let genDrawing (rng: Random) numShapes =
//     let defaultHeight = 10000
//     let defaultWidth = 15000

//     let shapes =
//         seq {
//             for _ in 0 .. rng.Next(numShapes) ->
//                 let kind = Shapes.Shape.GetRandomKind rng

//                 kind.GetRandom rng defaultWidth defaultHeight
//         }
//         |> List.ofSeq

//     let id = drawings.Length

//     // x_travel: 16.93, // 430mm; about 24328.41 steps
//     // y_travel: 11.69, // 297mm about 16798.53 steps
//     let drawing =
//         { Id = id
//           Dimensions =
//             { Height = defaultHeight
//               Width = defaultWidth }
//           Plotter = defaultPlotter
//           Shapes = shapes
//           Error = None }

//     recordDrawing drawing
//     drawing

// TODO[gastove|2024-08-08] Need a way to fetch plotter config.
//    
// let getConfigForPlotter (plotter: Common.Types.Plotter) =
//     Runner.Runner.NewFromEnv()
//     |> Result.bind (Runner.getConfig plotter)
//     |> Result.map KeepResult.ApiResult
//     |> Result.mapError KeepError.RunnerError

// |> Result.map KeepResult.ApiResult
// |> Result.mapError KeepError.OfRunnerError
// |> KeepResponse.OfResult

module KeepHandlers =
    type KeepHandler<'t> = 't -> KeepResponse

// TODO[gastove|2021-04-04] Get this to return a KeepResponse.
// let printDrawing drawingId =
//     handleContext (fun ctx ->
//         let logger = ctx.GetLogger("printDrawing")
//         logger.LogInformation("HERE WE GOOOOOO")

//         let maybeDrawing = getDrawingById drawingId
//         let maybeRunner = Runner.Runner.NewFromEnv()

//         match (maybeDrawing, maybeRunner) with
//         | Ok drawing, Ok runner ->
//             logger.LogInformation($"Drawing {drawingId} succesfully loaded!")

//             // TODO[gastove|2020-12-31] Running the drawing should absolutely be async.
//             match
//                 runner.PlotterReady drawing.Plotter
//                 |> Result.bind (Runner.run drawing)
//                 with
//             | Ok (result) ->
//                 logger.LogInformation("%A", result)
//                 ctx.SetStatusCode 202
//                 ctx.WriteTextAsync <| String.Join(",", result)
//             | Error (e) ->
//                 logger.LogError(e.ToString())
//                 ctx.SetStatusCode 500
//                 e |> string |> ctx.WriteTextAsync
//         | _, _ ->
//             logger.LogError("Shoot, no drawing with ID %i found in %A", drawingId, drawings)
//             ctx.SetStatusCode 500
//             ctx.WriteTextAsync "Drawing failed")

/// jsonHandler takes encoded JSON data and takes care of setting headers, byte
/// conversion, and response writing.
let inline jsonHandler data : HttpHandler =
    handleContext (fun ctx ->
        ctx.SetContentType "application/json; charset=utf-8"

        data
        |> string
        |> Encoding.UTF8.GetBytes
        |> ctx.WriteBytesAsync)

/// encodeJsonHandler takes raw data to be encoded and encodes it with the given
/// encoder. No, I couldn't figure out how to work "encoder" in to that sentence
/// more times, but thank you for asking.
///
/// RespondJson does everything you want it to: sets the content type, writes
/// the encoded string to an output stream. No need to pass through jsonHandler.
let inline encodeJsonHandler encoder data : HttpHandler =
    ThothSerializer.RespondJson data encoder

let keepResultHandler (result: Result<KeepResult, KeepError>) : HttpHandler =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        match result with
        | Ok _ -> ctx.SetStatusCode 200
        | Error _ -> ctx.SetStatusCode 500 // TODO[gastove|2023-05-13] This should be more sophisticated

        encodeJsonHandler KeepResponse.encoder result next ctx

// let apiResultHandler (result: Result<DrawBridge.Plotter.JsonRpc.V1.ApiResult, Runner.RunnerError>) : HttpHandler =
//     fun (next: HttpFunc) (ctx: HttpContext) ->
//         let data =
//             result
//             |> Result.map KeepResult.ApiResult
//             |> Result.mapError KeepError.RunnerError

//         keepResultHandler data next ctx

let handleSetPlotter =
    fun (next: HttpFunc) (ctx: HttpContext) ->
        let incomingPlotter =
            ctx.GetQueryStringValue "plotter"
            |> Result.bind DrawBridge.Common.Types.Plotter.FromString

        let result =
            match incomingPlotter with
            | Ok parsedPlotter ->
                configuredPlotter <- parsedPlotter

                $"Set plotter to {parsedPlotter}"
                |> KeepResult.Message
                |> Ok

            | Error e -> e |> KeepError.ServerError |> Error

        keepResultHandler result next ctx

// TODO[gastove|2021-04-04] This function just needs to be different.
// Hard-coding the plotter is no good, and I'm not confident about this whole
// "return a pair of thing and runner". If nothing else, it needs to return a
// Result, not a KeepResponse.
//
// let loadPlotterAndCheckStatus () =
//     match Runner.Runner.NewFromEnv() with
//     | None ->
//         { KeepResponse.Empty with
//               Error = "Can't reach the plotter" |> ServerError |> Some },
//         None
//     | Some (runner) ->
//         match runner.ReportPlotterStatus Common.Types.AxiDrawV3A3 with
//         | Ok (fwv) ->
//             { KeepResponse.Empty with
//                   Data = Some fwv },
//             Some runner
//         | Error (ev) ->
//             { KeepResponse.Empty with
//                   Error = ev |> RunnerError |> Some },
//             Some runner

// TODO[gastove|2021-04-04] rewrite this
// let plotterStatusHandler () =
//     let response, _ = loadPlotterAndCheckStatus ()
//     response |> jsonHandler

// TODO[gastove|2022-07-17] This is where we actually need more like respondWithSVG
// let respondWithDrawing (drawing: Plot) : HttpHandler =
//     let svgRenderer = Rendering.SvgRenderer.Create 1.0

//     handleContext (fun ctx ->

//         ctx.SetContentType "image/svg+xml"

//         task {
//             let! result =
//                 drawing.Shapes
//                 |> svgRenderer.Render
//                 |> ctx.WriteTextAsync

//             return result
//         })

// let renderDrawing (drawing: Plot) =
//     let svgRenderer = Rendering.SvgRenderer.Create 1.0

//     drawing.Shapes
//     |> svgRenderer.Render
//     |> KeepResult.Svg

let drawingNotFoundHandler id =
    $"No drawing with ID {id}"
    |> text
    |> HttpStatusCodeHandlers.RequestErrors.notFound

let listPlottersHandler =
    [ DrawBridge.Common.Types.AxiDrawV3
      DrawBridge.Common.Types.AxiDrawV3A3
      DrawBridge.Common.Types.AxiDrawV3XLX ]
    |> List.map (fun p -> $"{p}".ToLower())
    |> json

let errorHandler (ex: Exception) (logger: ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")

    clearResponse
    >=> setStatusCode 500
    >=> text ex.Message

let apiRoutes (rng: System.Random) =
    [ GET
      >=> choose [ subRoute "/plotter"
                   <| choose [ route "/get"
                               >=> warbler (fun _ -> text $"{configuredPlotter}" >=> setStatusCode 200)

                               route "/list" >=> listPlottersHandler

                               // route "/status"
                               // >=> warbler (fun _ -> plotterStatusHandler ())
                                ]

                   // route "/config/get"
                   // >=> warbler (fun _ ->
                   //     configuredPlotter
                   //     |> getConfigForPlotter
                   //     |> keepResultHandler)

                   // subRoute "/drawing"
                   // <| choose [ routef "/%i"
                   //             <| fun drawingId ->
                   //                 drawingId
                   //                 |> getDrawingById
                   //                 |> Result.map renderDrawing
                   //                 |> keepResultHandler

                   //             route "/generate"
                   //             >=> warbler (fun _ ->
                   //                 rng.Next(200)
                   //                 |> genDrawing rng
                   //                 |> renderDrawing
                   //                 |> Ok
                   //                 |> keepResultHandler)

                               // routef "/generate/%i" (fun numShapes -> genDrawing rng numShapes |> respondWithDrawing)
                   //             ]

                    ]
      POST
      >=> choose [ route "/plotter/set"
                   >=> warbler (fun _ -> handleSetPlotter)

                   // routef "/fetch/%i" (fun id ->
                   //     getDrawingById id
                   //     |> Result.map DrawingResponse
                   //     |> encodeJsonHandler KeepResponse.encoder)

                   // routef "/print/%i" printDrawing
                   ] ]

// TODO[gastove|2020-07-09] We're going to need more routes than this:
// - [ ] Dump config
// - [ ] Render config politely
// - [ ] Some amount of set config
// - [ ] A poll-able plotter status endpoint
let applicationRoutes (rng: System.Random) =
    choose [ GET
             >=> choose [ route "/"
                          >=> text (sprintf "no api here, but here's a number: %d" (rng.Next())) ]

             subRoute "/api" <| choose (apiRoutes rng)

             RequestErrors.NOT_FOUND "Not Found" ]

let configureCors (builder: CorsPolicyBuilder) =
    builder
        .WithOrigins("http://localhost:8080")
        .AllowAnyMethod()
        .AllowAnyHeader()
    |> ignore

let configureApp (app: IApplicationBuilder) =
    let env =
        app.ApplicationServices.GetService<IWebHostEnvironment>()

    (match env.IsDevelopment() with
     | true -> app.UseDeveloperExceptionPage()
     | false -> app.UseGiraffeErrorHandler(errorHandler))
        .UseCors(configureCors)
        .UseGiraffe(applicationRoutes (System.Random()))

let configureServices (services: IServiceCollection) =
    services
        .AddCors()
        .AddGiraffe()
        .AddSingleton<Json.ISerializer>(new ThothSerializer(SnakeCase))
    |> ignore

module Logging =
    open Serilog

    let configureLogging () =
        Log.Logger <-
            LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger()

module Main =
    open Serilog

    [<EntryPoint>]
    let main _ =
        Logging.configureLogging ()

        Host
            .CreateDefaultBuilder()
            .ConfigureWebHostDefaults(fun webHostBuilder ->
                webHostBuilder
                    .Configure(configureApp)
                    .ConfigureServices(configureServices)
                |> ignore)
            .UseSerilog(fun _ctx services cfg ->
                cfg
                    .Destructure
                    .FSharpTypes()
                    .ReadFrom.Services(services)
                    .Enrich.FromLogContext()
                    .WriteTo.Console()
                |> ignore)
            .Build()
            .Run()

        0
