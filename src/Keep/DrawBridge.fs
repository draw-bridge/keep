namespace Keep.DrawBridge

module Helpers =
    open DrawBridge

    // TODO[gastove|2024-08-08] Don't hard-code the plotter
    let loadPlotterAndCheckStatus () =
        Runner.NewFromEnv()
        |> Result.bind (Runner.reportPlotterStatus Common.Types.AxiDrawV3A3)
