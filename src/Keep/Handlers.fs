module Keep.Handlers

open System.Text

open Keep.DrawBridge

open Giraffe

/// jsonHandler takes encoded JSON data and takes care of setting headers, byte
/// conversion, and response writing.
let inline jsonHandler data : HttpHandler =
    handleContext (fun ctx ->
        ctx.SetContentType "application/json; charset=utf-8"

        data
        |> string
        |> Encoding.UTF8.GetBytes
        |> ctx.WriteBytesAsync)

// TODO[gastove|2024-08-08] This is where we need to pick up next.
// I have a Result<ApiResult, RunnerError>
// I need to get to something sensible I can send across the wire to the frontend.
let plotterStatusHandler () =
    let response = Helpers.loadPlotterAndCheckStatus ()
    response |> jsonHandler
    