{
  description = "This is a project. I hope.";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs"; };

  outputs = { self, nixpkgs }:

    let
      pkgs =
        nixpkgs.legacyPackages.x86_64-linux;

      dotnetSdks = (with pkgs.dotnetCorePackages; combinePackages [
        # sdk_6_0
        sdk_8_0
      ]);

    in
    {

      devShell.x86_64-linux =
        pkgs.mkShell {
          buildInputs = [
            dotnetSdks
            pkgs.just
          ];

          DOTNET_ROOT = "${dotnetSdks}";
        };
    };

}
