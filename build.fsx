#r "paket: groupref build //"
#load "./.fake/build.fsx/intellisense.fsx"

open Fake.Core
open Fake.IO
open Fake.IO.Globbing.Operators
open Fake.DotNet

let integrationTestContainer =
    "registry.gitlab.com/draw-bridge/draw-bridge/integration-reference"

let apiSpecVersion = "0.8.0"

let buildDir = "./target/"

let projects =
    !! "src/**/*.fsproj"
    |> Seq.filter (fun p -> p.Contains(".fable") |> not)

let setBuildArgs (input: MSBuildParams) =
    { input with DisableInternalBinLog = true }

//----------------------------- Utility Functions -----------------------------//
let makeDrawBridgeSocketDir () =
    System.Environment.GetEnvironmentVariable "XDG_RUNTIME_DIR"
    |> sprintf "%s/drawbridge"

let makeDrawBridgeSocket s = sprintf "%s/S.socket" s

let pullContainer ctn =
    CreateProcess.fromRawCommand "podman" [ "pull"; ctn ]
    |> Proc.run

let startDrawBridgeReferenceContainer () =
    let integrationContainer =
        sprintf "%s:%s" integrationTestContainer apiSpecVersion

    let socketDir = makeDrawBridgeSocketDir ()

    Shell.mkdir socketDir

    let containerEnvVarVal = makeDrawBridgeSocket "/tmp"

    let args =
        Arguments.Empty
        |> Arguments.appendRaw "run"
        |> Arguments.appendRaw "--privileged"
        |> Arguments.appendRaw "--detach"
        |> Arguments.appendRaw "--rm"
        |> Arguments.appendNotEmpty "-v" (sprintf "%s:/sockets" socketDir)
        |> Arguments.appendNotEmpty "-e" (sprintf "DRAWBRIDGE_SOCK=%s" containerEnvVarVal)
        |> Arguments.appendNotEmpty "--ipc" "host"
        |> Arguments.appendNotEmpty "--name" "keep-integration-test-db-reference"
        |> Arguments.appendRaw integrationContainer

    let cmd = RawCommand("podman", args)

    CreateProcess.fromCommand cmd
    |> CreateProcess.redirectOutput
    |> Proc.run

let stopContainer container =
    CreateProcess.fromRawCommand "podman" [ "stop"; container ]
    |> Proc.run

//---------------------------------- Targets ----------------------------------//
Target.create "Build"
<| fun _ -> DotNet.build (fun args -> { args with Configuration = DotNet.BuildConfiguration.Debug }) "keep.sln"

Target.create "EnsureNoReferenceContainerRunning"
<| fun _ ->
    let target = "keep-integration-test-db-reference"
    let result = stopContainer target

    // 125 means there was no container to stop
    if result.ExitCode <> 0 && result.ExitCode <> 125 then
        failwithf "Failed to stop container %s" target
    else
        Trace.logf "Container %s stopped" target

Target.create "RunReferenceIntegrationTests"
<| fun _ ->
    let socket =
        makeDrawBridgeSocketDir () |> makeDrawBridgeSocket

    let podStart = startDrawBridgeReferenceContainer ()

    if podStart.ExitCode <> 0 then
        failwithf "Reference container failed to start: %s" podStart.Result.Error

    let testResult =
        CreateProcess.fromRawCommand
            "dotnet"
            [ "run"
              "-p"
              "src/DrawBridge.IntegrationTest.ReferencePlotter" ]
        |> CreateProcess.setEnvironmentVariable "DRAWBRIDGE_SOCK" socket
        |> CreateProcess.redirectOutput
        |> Proc.run

    stopContainer "keep-integration-test-db-reference"
    |> ignore

    match testResult.ExitCode with
    | 0 -> Trace.log testResult.Result.Output
    | _ -> failwithf "Integration tests faied:\%A" testResult.Result.Output

Target.create "RunApiSpecIntegrationTests"
<| fun _ ->
    let args =
        Arguments.Empty
        |> Arguments.appendRaw "run"
        |> Arguments.appendNotEmpty "-v" (sprintf "%s:/app" <| Shell.pwd ())
        |> Arguments.appendRaw "--privileged"
        |> Arguments.appendRaw "--rm"
        |> Arguments.appendRaw "dbapiinteg:local"
        |> Arguments.append [ "dotnet"
                              "run"
                              "-p"
                              "/app/src/DrawBridge.IntegrationTest.ApiSpec/" ]

    let cmd = Command.RawCommand("podman", args)

    let result =
        cmd
        |> CreateProcess.fromCommand
        |> CreateProcess.redirectOutput
        |> Proc.run

    match result.ExitCode with
    | 0 -> Trace.log result.Result.Output
    | _ -> failwithf "API Spec Integration tests failed:\n%s" result.Result.Output

open Fake.Core.TargetOperators

"Build"
==> "EnsureNoReferenceContainerRunning"
==> "RunReferenceIntegrationTests"

"Build" ==> "RunApiSpecIntegrationTests"

Target.runOrDefault "Build"
